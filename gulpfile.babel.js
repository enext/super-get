import Taskerify from "taskerify";

Taskerify.config.sourcemaps = false;
Taskerify.config.srcPath = "./src/assets"; // Src Path
Taskerify.config.distPath = "./dist/assets"; // Dist Path
Taskerify.config.srcViewsPath = "./src"; // Views Src Path
Taskerify.config.distViewsPath = "./dist"; // Compiled Views Dist Path (HTML)

const NODE_MODULES = "./node_modules";
const SRC = Taskerify.config.srcPath;
const DIST = Taskerify.config.distPath;
const ARCHIVES = './dist/arquivos';
const FILES = './dist/files';

const storeName = "superget";
const commonFiles = ["globals", "checkout", "checkout-confirmation-custom"];
const desktopFiles = ["general", "home", "product", "account", "landing-app", "landing-superdigital", "landing-taxas-e-tarifas", "institutional","portabilidade", "landing-site-map","portabilidade-layout2", "landing-product", "cartao", "1cartao", "cadastro", "maquininha-cartao"];
const mobileFiles = desktopFiles;

Taskerify((mix) => {
    // PugJS Template
    mix.pug();

    // Javascript Linter
    mix.eslint();

    // SVG to Iconfonts
    mix.iconfont({
        /** Plugin options - Default Values */
        normalize: true,
        fontHeight: 1001,
        centerHorizontally: true,

        /** Fonts / CSS options */
        iconsPath: `${SRC}/common/icons/`,
        sassPath: `${SRC}/common/scss/settings/`,
        fontPath: "/arquivos/",
        outputFontPath: `${DIST}/common/iconfonts/`,
        className: "iconfont",
        iconFontName: `${storeName}-iconfonts`,
        template: `${SRC}/common/scss/utils/_iconfonts-template.scss`,
        sassFileName: "_iconfonts",
        customExtension: ".css"
    });

    // Image Minifier
    mix.imagemin(`${SRC}/common/images`, `${DIST}/common/images`);

    // Common Files
    commonFiles.map(file => {
        mix.browserify(
            `${SRC}/common/js/${storeName}-common-${file}.js`,
            ARCHIVES
        ).sass(
            `${SRC}/common/scss/${storeName}-common-${file}.scss`,
            ARCHIVES
        );
    });

    // Main Desktop Files
    desktopFiles.map(file => {
        mix.browserify(
            `${SRC}/desktop/js/${storeName}-desktop-${file}.js`,
            ARCHIVES
        ).sass(
            `${SRC}/desktop/scss/${storeName}-desktop-${file}.scss`,
            ARCHIVES
        );
    });

    // Main Mobile Files
    mobileFiles.map(file => {
        mix.browserify(
            `${SRC}/mobile/js/${storeName}-mobile-${file}.js`,
            ARCHIVES
        ).sass(
            `${SRC}/mobile/scss/${storeName}-mobile-${file}.scss`,
            ARCHIVES
        );
    });

    // Checkout
    mix.sass(`${SRC}/common/scss/checkout6-custom.scss`, FILES);
    mix.browserify(`${SRC}/common/js/modules/Checkout/checkout6-custom.js`, FILES)

    // Mix Vendor Scripts
    mix.scripts(
        [`${NODE_MODULES}/vtex-minicart/dist/vtex-minicart.js`],
        ARCHIVES
    );
    mix.vtex('superget');
});
