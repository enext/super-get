
import InstitutionalCommon from './../../../../common/js/modules/Institutional/institutional-index.js';

const init = () => {    
    InstitutionalCommon.init();    
};

export default {
    init: init,
};
