import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.home;

const Methods = {
    init() {
        Methods.openTaxas();
    },

    openTaxas() {
        El.btnTaxas.on('click', (ev) => {
            const $this = $(ev.currentTarget);
            El.modalTaxas.removeClass('is--hidden');
        });

        El.closeModal.on('click', (ev) => {
            const $this = $(ev.currentTarget);
            El.modalTaxas.addClass('is--hidden');
            El.modalTaxas2.removeClass('is--hidden');
            console.log('foi');
            return false;
        });

        El.modalTaxas2.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            El.modalTaxas2Wrapper.removeClass('is--hidden');
        });

        El.closeCondicoes.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            El.modalTaxas2Wrapper.addClass('is--hidden');
        })

    },
};

function openModalTaxas() {
    document.querySelector('#openModal').addEventListener('click', function () {
        document.querySelector('.x-modal').style.display = 'block';
    })
}

export default {
    init: Methods.init,
};