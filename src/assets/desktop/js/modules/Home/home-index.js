
import HomeMainBanner from './_home__main-banner.js';
import HomeMainShelf from './_home__main-shelf.js';
import HomeMainDepoimentos from './_home__depoimentos.js';
import HomeMainFaq from './_home__faq.js';
import HomeMainOpenTaxas from './_home__taxas.js';
import HomeSlideProducts from './_home_slide-products.js';
import HomeCartao from './_home_cartao.js';
//import HomeHeaderCredenciamento from './_home__header-credenciamento';

const init = () => {
    HomeMainBanner.init();
    HomeMainDepoimentos.init();
    HomeMainShelf.init();
    HomeMainFaq.init();
    HomeMainOpenTaxas.init();
    HomeSlideProducts.init();
    HomeCartao.init();
    //HomeHeaderCredenciamento.init();
};

export default {
    init: init,
};
