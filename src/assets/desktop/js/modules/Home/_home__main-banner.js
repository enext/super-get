import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.home.mainBanners;

const Methods = {
    init() {
        Methods.banners();
        if (window.location.search.includes('?&utm_source=facebook%20utm_campaign=superget_facebook_social_conversao_audience-teste-ab_tribal')) {
            Methods.trocaBanner();
        }
    },

    banners() {
        const AUTOPLAY_SPEED = 13000;
        const SPEED = 1000;

        El.big.slick({
                autoplay: true,
                autoplaySpeed: AUTOPLAY_SPEED,
                arrows: true,
                dots: true,
                speed: SPEED,
                fade: false,
                cssEase: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
                lazyLoad: VTEX.slickLazyDesktop,
                pauseOnHover: true,
            })
            .on('lazyLoaded', (event, slick, image, imageSource) => {
                $(image).removeClass('has--placeloader');
            });
            
// document.querySelectorAll('.box-banner a > img[alt="JovemPan"]')[1].parentNode.setAttribute('target', '_blank');
    },

    trocaBanner() {
        const imagem = document.getElementById('ihttps://superget.vteximg.com.br/arquivos/ids/155544/2-PFM.png?v=636964552600930000')
        imagem.src = 'https://superget.vteximg.com.br/arquivos/taxa-unificada.gif'
    }
};

export default {
    init: Methods.init,
};
