const Methods = {
  init() {
    Methods.openModalCartao();
  },

  openModalCartao() {
    let modal = $(".x-superdigital-modal");

    $(".x-superdigital-cartao__content__cta button").on('click', () => {
      modal.css({ "display": "flex" });
      $("body").addClass("x-hidecartao-digital");
    })

    $(".x-superdigital-modal__box__exit a").on('click', function (e) {
      e.preventDefault();
      modal.css({ "display": "none" });
      $("body").removeClass("x-hidecartao-digital");
    })
  }
}

export default {
  init: Methods.init,
};