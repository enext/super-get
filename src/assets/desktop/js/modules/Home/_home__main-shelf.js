import Constants from './../../../../common/js/utils/constants.js';
import CacheSelectors from './__cache-selectors';

const Arrow = Constants.slickArrow;
const El = CacheSelectors.home;
const Methods = {
  init() {
    Methods.mainShelf();
    Methods.comparativeShelf();
  },

  comparativeShelf() {
    const textList = $('.x-comparative__text ul li'),
          advantagesList = $('.x-comparative__ideal ul'),
          svgsList = $('.x-comparative__svgs ul'),
          svgs = $('.x-comparative__product__topics'),
          advantages = $('.x-comparative__product__advantages'),
          text = $('.x-comparative__product__text');

    
    advantages.each(function (index) {
      advantages.eq(index).append(advantagesList.eq(index));
      text.eq(index).html(textList.eq(index).text());
      svgs.eq(index).html(svgsList.eq(index));
    })
  },

  mainShelf() {
    $(document).on('shelfCarouselEnd.vtexShelfProperties', (ev) => {
      if (!El.mainShelf.hasClass('slick-initialized')) {
        El.mainShelf.slick({
          autoplay: true,
          slidesToShow: 4,
          arrows: true,
          dots: false,
          prevArrow: `<button type="button" class="slick-prev">${Arrow}</button>`,
          nextArrow: `<button type="button" class="slick-next">${Arrow}</button>`,
          lazyLoad: VTEX.slickLazyDesktop,
        })
          .on('lazyLoaded', (event, slick, image, imageSource) => $(image).removeClass('has--placeloader'));
      }
    });
  },
};



export default {
  init: Methods.init,
};
