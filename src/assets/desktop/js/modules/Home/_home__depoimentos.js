
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.home.testimonials;

const Methods = {
    init() {
        Methods.testimonials();
    },

    testimonials() {
        if ( ! El.hasClass('slick-initialized') ) {
            El.slick({
                dots: false,
                slidesToShow: 3,
                slidesToScroll:1
            });
        }
    },
};

export default {
    init: Methods.init,
};
