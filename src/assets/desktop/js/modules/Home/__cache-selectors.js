export default {
    home: {
        mainBanners: {
            big: $('.x-banner__contents')
        },
        mainShelf: $('.js--home-shelf .js--shelf ul'),
        testimonials: $('.x-testimonials__items'),
        faqTitle: $('.js--faq-title'),
        btnTaxas: $('.js--open-taxas'),
        modalTaxas: $('.x-modal'),
        modalTaxas2: $('.js-trigger-modal'),
        modalTaxas2Wrapper: $('.modal-containter'),
        closeModal: $('.js-modal-close'),
        closeCondicoes: $('.js-modal'),
        slideProducts: $("#x-shelf > div > div.x-shelf__contents > div > div > ul"), 
    },
};