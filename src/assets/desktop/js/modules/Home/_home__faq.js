
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.home.faqTitle;

const Methods = {
    init() {
        Methods.faqTitle();
    },

    faqTitle() {
        El.live('click', function(e){
            $(this).toggleClass('is--active');
            $(this).next().slideToggle();
        });
    },
};

export default {
    init: Methods.init,
};
