import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.home.slideProducts;

const Methods = {
    init() {
        Methods.slideProducts();
    },

    slideProducts() {
        El.slick({
            arrows: true,
        })
    },
};

export default {
    init: Methods.init,
};
