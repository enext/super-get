
import InstitutionalCommon from './portabilidade__main.js';

const init = () => {    
    InstitutionalCommon.init();    
};

export default {
    init: init,
};
