import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.portabilidade;

const Methods = {
    init() {
        Methods.actionsPage();
        Methods.actionModal();
        Methods.actionBusiness();
    },

    actionsPage() {
        El.accordionItems.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            $this.addClass('is--active').parent().next(El.accordionResult).slideToggle();
        });
    },

    actionModal() {
        El.btnModal.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            const targetId = $this.attr('data-modal');
            $(`.port-c-modal#${targetId}`).addClass('is--active');
        });

        El.btnOpenModalTaxas.on('click', (ev) => {
            ev.preventDefault();
            console.log('clicou');
            document.querySelector('.x-modal').classList.remove('is--hidden');
            document.querySelector('.x-modal').classList.add('is--active');
        });

        $('.splp-c-modal__close.js-modal-close').on('click', function() {
            $('#modal-taxas').removeClass('is--active');
        });
    },

    actionBusiness() {
        El.actionBusiness.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            const idTarget = $this.attr('id');
            $this.addClass('is-active').siblings().removeClass('is-active');
            $this.closest('.port-c-steps__tabs').find(`div[aria-labelledby=${idTarget}]`).addClass('is-active').siblings().removeClass('is-active');
        });
    },
};

export default {
    init: Methods.init,
};