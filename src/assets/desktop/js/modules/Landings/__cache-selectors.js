
export default {
    portabilidade: {
        accordionItems: $('.js-open-faq'),
        accordionResult: $('.port-c-ajuda__content'),
        modal: $('.port-c-modal'),
        btnModal: $('.js-trigger-modal'),
        closeModal: $('.js-modal-close'),
        actionBusiness: $('.js-show-step'),
        btnOpenModalTaxas: $('.js--open-taxas'),
    },
};
