
import ProductCommon from './../../../../common/js/modules/Product/index.js';
import ProductImg from './product-img.js';
import HomeMainDepoimentos from '../Home/_home__depoimentos';
import HomeMainFaq from '../Home/_home__faq.js';
import HomeMainOpenTaxas from '../Home/_home__taxas.js';

const init = () => {    
    ProductCommon.init();
    ProductImg.init();
    HomeMainDepoimentos.init();
    HomeMainFaq.init();
    HomeMainOpenTaxas.init();
};
export default {
    init: init,
};
