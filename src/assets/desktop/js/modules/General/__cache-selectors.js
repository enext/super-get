
export default {
    preHeader: {
        self: $('.js--pre-header'),
        list: $('.js--pre-header ul'),
        close: $('.js--pre-header__close'),
    },
};
