
import GeneralMain from './_general__main.js';

const init = () => {
    GeneralMain.init();
};

export default {
    init: init,
};
