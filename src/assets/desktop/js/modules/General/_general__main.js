
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.preHeader;
const Methods = {
    init() {
        Methods.preHeaderList();
        Methods.preHeaderClose();
        Methods.toggleAtendimento();
        Methods.hideButtonPortabilidade();
    },

    preHeaderList() {
        El.list.slick({
            arrows: false,
            autoplay: true,
            autoplaySpeed: 0,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 5000,
        });
    },
    preHeaderClose() {
        El.close.on('click', (_ev) => $.publish('/superget/closePreHeader'));
        $.subscribe('/superget/closePreHeader', (_ev) => El.self.removeClass('is--active'));
    },
    hideButtonPortabilidade(){
        var url_atual = window.location.href;
        if(url_atual==="https://www.superget.com.br/portabilidade"){
            
            var headerList = document.querySelector('.x-header__info--list');
            headerList.style.justifyContent="space-around";

            var btnPortabilidade = document.querySelector('.x-header__info--item:nth-of-type(2)');
            btnPortabilidade.style.display="none";

            console.log("portabilidade")
        }else{
            console.log(url_atual);
        }
    },

    toggleAtendimento() {
        $('.js-atendimento').live('click', function(){
            $('.x-atendimento__icones').toggleClass('is--active');
        });
    }
    
};

export default {
    init: Methods.init,
};
