! function(e) {
    function t(t) { for (var n, c, i = t[0], l = t[1], u = t[2], d = 0, f = []; d < i.length; d++) c = i[d], r[c] && f.push(r[c][0]), r[c] = 0; for (n in l) Object.prototype.hasOwnProperty.call(l, n) && (e[n] = l[n]); for (s && s(t); f.length;) f.shift()(); return a.push.apply(a, u || []), o() }

    function o() {
        for (var e, t = 0; t < a.length; t++) {
            for (var o = a[t], n = !0, i = 1; i < o.length; i++) {
                var l = o[i];
                0 !== r[l] && (n = !1)
            }
            n && (a.splice(t--, 1), e = c(c.s = o[0]))
        }
        return e
    }
    var n = {},
        r = { 0: 0 },
        a = [];

    function c(t) { if (n[t]) return n[t].exports; var o = n[t] = { i: t, l: !1, exports: {} }; return e[t].call(o.exports, o, o.exports, c), o.l = !0, o.exports }
    c.m = e, c.c = n, c.d = function(e, t, o) { c.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: o }) }, c.r = function(e) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(e, "__esModule", { value: !0 }) }, c.t = function(e, t) {
        if (1 & t && (e = c(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var o = Object.create(null);
        if (c.r(o), Object.defineProperty(o, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e)
            for (var n in e) c.d(o, n, function(t) { return e[t] }.bind(null, n));
        return o
    }, c.n = function(e) { var t = e && e.__esModule ? function() { return e.default } : function() { return e }; return c.d(t, "a", t), t }, c.o = function(e, t) { return Object.prototype.hasOwnProperty.call(e, t) }, c.p = "";
    var i = window.webpackJsonp = window.webpackJsonp || [],
        l = i.push.bind(i);
    i.push = t, i = i.slice();
    for (var u = 0; u < i.length; u++) t(i[u]);
    var s = l;
    a.push([86, 1]), o()
}({
    83: function(e, t, o) {},
    86: function(e, t, o) {
        "use strict";
        o.r(t);
        o(51), o(59), o(62), o(64), o(66), o(67), o(70), o(47), o(30), o(49), o(50), o(32);

        function n(e, t) { return e.classList ? e.classList.contains(t) : new RegExp("(^| )".concat(t, "( |$)"), "gi").test(e.className) }

        function r(e, t) { if (n(e, t)) return !1; if (e.classList) return e.classList.add(t); var o = "".concat(e.className, " ").concat(t); return e.className = o, o }

        function a(e, t) { if (!e) return !1; if (!n(e, t)) return !1; if (e.classList) return e.classList.remove(t); var o = new RegExp("(^|\\b)".concat(t.split(" ").join("|"), "(\\b|$)"), "gi"); return e.className = e.className.replace(o, " "), !0 }

        function c(e, t) { if (!e) return null; for (var o = null, r = e.parentNode, a = 0; null === o && !((a += 1) > 3e3) && r && ("BODY" !== r.tagName || n(r, t));) n(r, t) ? o = r : r = r.parentNode; return o }

        function i(e, t) { return n(e, t) ? e : c(e, t) }

        function l(e, t) { for (var o = 0; o < e.length; o += 1) t(e[o]) }

        function u(e, t, o) { l(e, function(e) { return e.addEventListener(t, o) }) }
        o(83);
        document.querySelector(".js-open-menu").addEventListener("click", function(e) { e.preventDefault(), r(document.body, "is-menu-active") }), document.querySelector(".js-close-menu").addEventListener("click", function(e) { e.preventDefault(), a(document.body, "is-menu-active") }), u(document.querySelectorAll(".js-navigate-to"), "click", function(e) { a(document.body, "is-menu-active") });
        var s = document.querySelectorAll(".js-trigger-ga-event");
        if (s && u(s, "click", function(e) {
                var t = function(e) {
                        for (var t = e.attributes, o = {}, n = 0; n < t.length; n += 1) {
                            var r = t[n],
                                a = r.name;
                            a.includes("data-") && (o[a.replace("data-", "").split("-").reduce(function(e, t, o) { return 0 === o ? t.toLowerCase() : e + (t = t[0].toUpperCase() + t.slice(1, t.length)) }, "")] = r.value)
                        }
                        return o
                    }(i(e.target, "js-trigger-ga-event")),
                    o = { event: "gaevent", eventcategory: t.eventcategory, eventaction: t.eventaction, eventlabel: t.eventlabel };
                console.log("Pushing to GA dataLayer", o), "undefined" != typeof dataLayer && void 0 !== dataLayer.push && dataLayer.push(o)
            }), u(document.querySelectorAll(".js-trigger-modal"), "click", function(e) {
                e.preventDefault();
                var t = i(e.target, "js-trigger-modal").getAttribute("data-modal"),
                    o = document.querySelector("#".concat(t));
                o && (console.log("yay"), r(o, "port-c-modal--active"), "undefined" != typeof dataLayer && void 0 !== dataLayer.push && dataLayer.push({ event: "trackPage", PNPagina: "superget/".concat(t) }))
            }), u(document.querySelectorAll(".js-modal-close"), "click", function(e) {
                e.preventDefault();
                var t = c(i(e.target, "js-modal-close"), "port-c-modal");
                t && (a(t, "port-c-modal--active"), "undefined" != typeof dataLayer && void 0 !== dataLayer.push && dataLayer.push({ event: "trackPage", PNPagina: "superget" }))
            }), u(document.querySelectorAll(".js-toggle-video"), "click", function(e) {
                e.preventDefault();
                var t = i(e.target, "js-toggle-video"),
                    o = document.querySelector(".port-c-m-video__btn--active"),
                    n = t.classList.contains("port-c-m-video__btn--active"),
                    c = t.classList.contains("port-c-m-video__btn--android"),
                    l = t.classList.contains("port-c-m-video__btn--ios"),
                    u = document.querySelector("#video-android"),
                    s = document.querySelector("#video-ios");
                n || (a(o, "port-c-m-video__btn--active"), r(t, "port-c-m-video__btn--active"), l && (a(u, "show-video"), r(s, "show-video")), c && (a(s, "show-video"), r(u, "show-video")))
            }), window.matchMedia("(max-width: 767px)").matches) {
            var d = document.querySelector(".port-c-steps__tabs");
            d.addEventListener("touchend", function() {
                var e = document.querySelector(".port-c-steps__step-by-step").clientWidth,
                    t = document.querySelectorAll(".js-show-step"),
                    o = document.querySelectorAll(".js-show-step-content"),
                    n = 100 * d.scrollLeft / (d.scrollWidth - d.clientWidth);
                l(t, function(e) { a(e, "is-active") }), l(o, function(e) { a(e, "is-active") }), n <= 12.5 ? (r(t[0], "is-active"), d.scroll({ left: 0, behavior: "smooth" })) : n > 12.5 && n <= 37.5 ? (r(t[1], "is-active"), d.scroll({ left: e / 4 - 30, behavior: "smooth" })) : n > 37.5 && n <= 82.5 ? (r(t[2], "is-active"), d.scroll({ left: e / 2 - 30, behavior: "smooth" })) : (r(t[3], "is-active"), d.scroll({ left: e, behavior: "smooth" }));
                var c = document.querySelector(".js-show-step.is-active").id;
                r(document.querySelector('.js-show-step-content[aria-labelledby="' + c + '"]'), "is-active")
            })
        }
        var f = o(19),
            v = o.n(f);
        v.a.polyfill(), u(document.querySelectorAll(".js-show-info"), "click", function(e) {
            e.preventDefault();
            var t = i(e.target, "js-show-info"),
                o = c(t, "port-c-escolha__item"),
                l = o.querySelector(".port-c-escolha__item__infos"),
                u = window.pageYOffset || document.documentElement.scrollTop,
                s = l.getBoundingClientRect().top + u - 112;
            if (window.scroll({ top: s, left: 0, behavior: "smooth" }), n(l, "is-open")) return s = o.getBoundingClientRect().top + u - 72, t.innerHTML = "veja mais informações", a(l, "is-open"), void window.scroll({ top: s, left: 0, behavior: "smooth" });
            t.innerHTML = "ocultar informações", r(l, "is-open")
        }), u(document.querySelectorAll(".js-open-faq"), "click", function(e) {
            e.preventDefault();
            var t = function(e, t) { if (!e) return null; if (e.tagName === t) return e; for (var o = null, n = e.parentNode, r = 0; null === o && !((r += 1) > 3e3) && n && ("BODY" !== n.tagName || "BODY" === t);) n.tagName !== t ? n = n.parentNode : o = n; return o }(i(e.target, "js-open-faq"), "LI");
            n(t, "is-open") ? a(t, "is-open") : r(t, "is-open")
        }), v.a.polyfill(), u(document.querySelectorAll(".js-navigate-to"), "click", function(e) {
            // e.preventDefault();
            var t = i(e.target, "js-navigate-to").getAttribute("href"),
                o = document.querySelector(t),
                n = o.offsetTop - o.scrollTop + o.clientLeft;
            window.scroll({ top: n, left: 0, behavior: "smooth" })
        });
        o(84);

        function p() {
            var e = window.scrollY,
                t = function() {
                    var e = document.querySelectorAll(".port-main-header__nav-items .js-navigate-to"),
                        t = {};
                    return l(e, function(e) {
                        var o, n = e.getAttribute("href"),
                            r = (o = document.querySelector(n)).offsetTop - o.scrollTop + o.clientLeft - 62;
                        t[n] = r
                    }), t
                }(),
                o = Object.values(t),
                n = Object.keys(t),
                r = null;
            return e < o[0] ? n[0] : (o.forEach(function(t, a) {
                if (!r) {
                    var c = o[a + 1];
                    void 0 !== c && (e < t || e >= c) || (r = n[a])
                }
            }), r)
        }

        function m() {
            var e = p(),
                t = document.querySelector('.port-main-header__nav-items a[href="'.concat(e, '"]')),
                o = document.querySelector(".port-main-header__nav-items .is-active a");
            o && a(o.parentNode, "is-active"), r(t.parentNode, "is-active")
        }
        // window.addEventListener("scroll", m), m(), setTimeout(function() { r(document.body, "animations-enabled") }, 200)
    }
});
//# sourceMappingURL=app.37153b92.js.map