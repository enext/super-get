export default {
   init() {
      faq();
   }
}

function faq() {
   $(".x-superget-card__know-more").on("click", event => {
      event.preventDefault();

      $(".x-superget-card__pop-up").removeClass("is--hidden");
      $("html").addClass("blocked-buy-pop-up");
   });

   $(".x-superget-card__pop-up__close-button").on("click", event => {
      event.preventDefault();

      $(".x-superget-card__pop-up").addClass("is--hidden");
      $("html").removeClass("blocked-buy-pop-up");
   });

   $(".x-superget-card__pop-up__overlay").on("click", event => {
      event.preventDefault();

      $(".x-superget-card__pop-up").addClass("is--hidden");
      $("html").removeClass("blocked-buy-pop-up");
   });
}