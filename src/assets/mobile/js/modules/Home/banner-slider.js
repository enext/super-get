export default {
    init() {
        bannerSlider();
    }
}

function bannerSlider() {
    const $bannerEl = $('#mainBanner');
    const AUTOPLAY_SPEED = 13000;
    const SPEED = 1000;
    const settings = {
        autoplay: true,
        autoplaySpeed: AUTOPLAY_SPEED,
        speed: SPEED,
        infinite: true,
        dots: true,
        ease: 'cubic-bezier(0.4, 0.0, 1, 1)',
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true,
        touchThreshold: 20,
        arrows: false,
    }

    $bannerEl
        .slick(settings);
        
// document.querySelectorAll('.box-banner a > img[alt="JovemPan"]')[1].parentNode.setAttribute('target', '_blank');
}