import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.home.slideProducts;
const Helper = CacheSelectors.home.helperComplement;

const Methods = {
    init() {
        Methods.slideProducts();
    },

    slideProducts() {
        if (!El.hasClass("slick-initialized")) {
            $('.helperComplement').remove();
                El.slick({
                arrows: true,
                infinite:true,
            }); 
        }
    },
};

export default {
    init: Methods.init,
};
