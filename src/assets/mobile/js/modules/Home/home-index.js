import bannerSlider  from './banner-slider.js';
import benefitsSlider  from './benefits-slider.js';
import testimonialSlider  from './testimonial-slider.js';
import faq  from './faq.js';
import HomeMainOpenTaxas from './_home__taxas.js';
import HomeSlideProducts from './_home_slide-products.js'
import supergetCardPopup from './superget-card-popup.js';
import Comparative from './home-comparative.js';
//import HomeHeaderCredenciamento from './_home__header_credenciamento';

const init = () => {
    bannerSlider.init();
    benefitsSlider.init();
    testimonialSlider.init();
    faq.init();
    HomeMainOpenTaxas.init();
    HomeSlideProducts.init();
    supergetCardPopup.init();
    Comparative.init();
   // HomeHeaderCredenciamento.init();
};  

export default {
    init: init,
};
