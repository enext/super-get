export default {
    init() {
        faq();
    }
}

function faq() {
    $('.js--faq-title').live('click', function(e){
        $(this).toggleClass('is--active');
        $(this).next().slideToggle();
    });
}