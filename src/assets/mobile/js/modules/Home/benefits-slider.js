export default {
    init() {
        benefitsSlider();
    }
}

function benefitsSlider() {
    const $benefitsEl = $('#benefitsList');     
    const SPEED = 1000;
    const settings = {                
        // speed: SPEED,
        infinite: true,
        dots: true,        
        slidesToShow: 3,
        slidesToScroll: 3,
        // swipeToSlide: true,
        // touchThreshold: 50,
        arrows: true,
        // swipe: true,
        // touchMove: true,
    }

    $benefitsEl
        .slick(settings);
}