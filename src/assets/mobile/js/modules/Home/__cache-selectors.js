export default {
    home: {
        btnTaxas: $('.js--open-taxas'),
        modalTaxas: $('.x-modal'),
        closeModal: $('.js-modal-close'),
        modalTaxas2: $('.js-trigger-modal'),
        modalTaxas2Wrapper: $('.modal-containter'),
        closeModal: $('.js-modal-close'),
        closeCondicoes: $('.js-modal'),
        slideProducts: $("#x-shelf > div > div.x-shelf__contents > div > div > ul"), 
        helperComplement: $('.helperComplement'),
    },
};