export default {
    init() {
        testimonialSlider();
    }
}

function testimonialSlider() {
    const $testimonialEl = $('#testimonialList'); 
    const AUTOPLAY_SPEED = 2500;      
    const SPEED = 1000;
    const settings = {                
        autoplay: true,
        autoplaySpeed: AUTOPLAY_SPEED,
        infinite: true,
        dots: false,        
        slidesToShow: 1,
        slidesToScroll: 1,        
        swipeToSlide: true,
        touchThreshold: 50,
        arrows: false,
        swipe: true,
        touchMove: true,
    }

    $testimonialEl
        .slick(settings);
}