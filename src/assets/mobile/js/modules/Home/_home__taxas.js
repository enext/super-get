import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.home;

const Methods = {
    init() {
        Methods.openTaxas();
        Methods.openInfo();
    },

    openTaxas() {
        El.btnTaxas.on('click', (ev) => {
            const $this = $(ev.currentTarget);
            El.modalTaxas.removeClass('is--hidden');
        });

        El.closeModal.on('click', (ev) => {
            const $this = $(ev.currentTarget);
            El.modalTaxas.addClass('is--hidden');
            El.modalTaxas2.removeClass('is--hidden');
            console.log('foi');
            return false;
        });

        El.modalTaxas2.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            El.modalTaxas2Wrapper.removeClass('is--hidden');
        });

        El.closeCondicoes.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            El.modalTaxas2Wrapper.addClass('is--hidden');
        })

    },
    openInfo(){
        $('.showInfo').on("click", function() {
	        if($('.x-superget-sell__item').hasClass('is--active')){
    	        $('.x-superget-sell__item').removeClass('is--active')
	        };
            $(this).parents('.x-superget-sell__item').toggleClass('is--active');
        });
        $('.infoText .close').on("click", function(){
            $(this).parents('.x-superget-sell__item').removeClass('is--active');
        });
    },
};



export default {
    init: Methods.init,
};