export default {
    init() {
        comparativeShelf();
    }
}
function comparativeShelf() {
    const textList = $('.x-comparative__text ul li'),
        advantagesList = $('.x-comparative__ideal ul'),
        svgsList = $('.x-comparative__svgs ul'),
        svgs = $('.x-comparative__product__topics'),
        advantages = $('.x-comparative__product__advantages'),
        text = $('.x-comparative__product__text');


    advantages.each(function (index) {
        advantages.eq(index).append(advantagesList.eq(index));
        text.eq(index).html(textList.eq(index).text());
        svgs.eq(index).html(svgsList.eq(index));
    })
}