
import ProductCommon from './../../../../common/js/modules/Product/index.js';
import ProductImg from './product-img.js';
import testimonialSlider  from '../Home/testimonial-slider.js';
import faq  from '../Home/faq.js';
import HomeMainOpenTaxas from '../Home/_home__taxas.js';

const init = () => {    
    ProductCommon.init();
    ProductImg.init();
    testimonialSlider.init();
    faq.init();
    HomeMainOpenTaxas.init();
};
export default {
    init: init,
};
