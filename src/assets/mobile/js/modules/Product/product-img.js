
import { product as El } from './../../../../common/js/modules/Globals/selectors';

export default {
    init() {
        imgSlide();
    },
};

function imgSlide() {      
    $(document).on('FETCHING_PRODUCT_DATABIND', (ev) => {
        !El.$productImg.hasClass('slick-initialized') && El.$productMainImg.slick({
            arrows: true,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,            
            infinite: false,          
            cssEase: 'cubic-bezier(0.0, 0.0, 0.2, 1)',
            swipeToSlide: true,
            touchThreshold: 100,
        })       
    });
}
