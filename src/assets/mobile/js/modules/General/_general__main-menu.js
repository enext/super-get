
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.header;
const Methods = {
    init() {
        Methods.openMenuMobile();
        Methods.closeMenuMobile();
        Methods.openSubmenuMobile();       
    },

    openMenuMobile() {
        El.menuMobileBtn.on('click', (ev) => {            
            VTEX.closeMenus();
            VTEX.disableScroll();
            El.menuMobileBtn.removeClass('is--active');
            VTEX.menuOverlay
                .add(VTEX.menuMobile)
                .add(El.menuMobileClose)                
                .addClass('is--active');            
        });
    },

    closeMenuMobile() {
        El.menuMobileClose.on('click', (ev) => {
            VTEX.closeMenus(true);
            El.menuMobileClose.removeClass('is--active');
            El.menuMobileBtn.addClass('is--active');
        });
    },    

    openSubmenuMobile() {
        $('.x-main-menu__drop-title').on('click',function (ev) {
            ev.preventDefault();
            ev.stopPropagation();

            const curr = $(ev.currentTarget);
            curr.toggleClass('is--active');
            $('.js--main-menu-shop').slideToggle();
        });
    },    
};

export default {
    init: Methods.init,
};
