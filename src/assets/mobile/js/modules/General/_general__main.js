
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.main;
const Methods = {
    init() {
        Methods.main();
    },

    main() {},
};

export default {
    init: Methods.init,
};
