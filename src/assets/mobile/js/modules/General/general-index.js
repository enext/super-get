
import GeneralMain from './_general__main.js';
import GeneralHeader from './_general__header.js';
import GeneralMainMenu from './_general__main-menu.js';

const init = () => {    
    GeneralMain.init();    
    GeneralHeader.init();
    GeneralMainMenu.init();
};

export default {
    init: init,
};
