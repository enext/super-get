
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.header;
const Methods = {
    init() {
        // Methods.preHeader();
        // Methods.preHeaderClose();
        Methods.menuACtions();
        Methods.minicart();
        Methods.openMenu();
        Methods.openSubmenu();
        Methods.toggleAtendimento();
        Methods.insertMenuHamburguer();
    },

    insertMenuHamburguer() {
        $('body').addClass('AB-Menu_hamburguer');
    },

    preHeader() {
        El.preHeader.slick({
            arrows: false,
            autoplay: true,
            autoplaySpeed: 0,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 5000,
            variableWidth: true,
        });
    },

    preHeaderClose() {
        El.menuMobileClose.on('click', (_ev) => $.publish('/superget/closePreHeader'));
        $.subscribe('/superget/closePreHeader', (_ev) => El.self.removeClass('is--active'));
    },

    menuACtions() {
        $('.js--menu-mobile-btn').live('click', function() {
            $('.js--header, .x-utilities__item').addClass('is--active');
            $('body').addClass('has--no-scroll');
        });
        $('.js--menu-mobile-close').live('click', function() {
            $('.js--header, .x-utilities__item').removeClass('is--active');
            $('body').removeClass('has--no-scroll');
        });
    },

    openMenu() {
        $('.x-header__menu--mobile').live('click', function() {
            $(this).toggleClass('is--active');
            $('body').toggleClass('has--no-scroll');

                $(this).hasClass('is--active') ?
                    $('.x-header').addClass('overlay')
                    :
                    $('.x-header').removeClass('overlay');
        });
    },

    minicart() {
        $('.js--minicart-open').live('click', function() {
            $('.x-minicart.js--minicart').toggleClass('is--active');
            $('body').toggleClass('has--no-scroll');
        });
    },

    openSubmenu() {
        $('.x-header__navigation--item').live('click', function() {
            $(this).find('.x-header__menu').slideToggle();
            $('body').toggleClass('has--no-scroll');
        });
    },

    toggleAtendimento() {
        $('.js-atendimento').live('click', function() {
            $('.x-atendimento__icones').toggleClass('is--active');
        });

        $('.js-atendimento__box .x-atendimento__icones--box').live('click', function() {
            $(this).toggleClass('is--active');
        });
    },
};

export default {
    init: Methods.init,
};

