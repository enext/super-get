const Methods = {
   init() {
      Methods.bannerCredencyMobile();
   },

   bannerCredencyMobile: function() {
      $(".x-credencia-mobile__range").on("input", function() {
         if (+this.value === 100) {
            document.querySelector(".x-credencia-mobile-redirect").click();
         }
      });
   }
};

document.addEventListener('DOMContentLoaded', function () {
   Methods.init();
});