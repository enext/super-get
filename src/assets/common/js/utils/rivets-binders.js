
/**
 * Listen on event and immediately stop its propagation
 *
 * @example
 *     <div rv-on-click-stop="notify"></div>
 */
rivets.binders['on-*-stop'] = {
    function: true,
    priority: rivets.binders['on-*'].priority + 1,

    unbind: function(el) {
        if ( this.handler ) {
            $.off(this.handler);
            this.handler = null;
        }
    },

    routine: function(el, value) {
        let self = this;
        if ( this.handler ) {
            $.off(this.handler);
        }

        $(el).on(self.args[0], self.handler = self.eventHandler(value));
    },
};

/**
 * Add class conditionally
 *
 * @example
 *     <div rv-add-class="is--active"></div>
 *     //=> <div class="is--active"></div>
 */
rivets.binders['add-class'] = ($el, value) => {
    if ( $el.addedClass ) {
        $el.classList.remove($el.addedClass);
        delete $el.addedClass;
    }

    if ( value ) {
        $el.classList.add(value);
        $el.addedClass = value;
    }
};

/**
 * Remove given class name by conditionals
 *
 * @example
 *     <div class="is--active" rv-remove-class-is--active="true"></div>
 *     //=> <div class></div>
 */
rivets.binders['remove-class-*'] = function($el, conditional) {
    if ( conditional ) {
        $el.classList.remove(this.args[0]);
    }
};

/**
 * Toggle given class name by conditionals
 *
 * @param  {Boolean}   conditional  `true` for add class, `false` for remove class
 * @example
 *     <div class="foo" rv-toggle-class-is--active="true"></div>
 *     //=> <div class="foo is--active"></div>
 *
 *     <div class="foo is--active" rv-toggle-class-is--active="false"></div>
 *     //=> <div class="foo"></div>
 */
rivets.binders['toggle-class-*'] = function($el, conditional) {
    if ( conditional ) {
        $el.classList.add(this.args[0]);
    } else {
        $el.classList.remove(this.args[0]);
    }
};
