
export default {
    contactFormValidate(contact) {
        const self = this;
        const customRules = {
            Telefone: 'required',
            subject: 'required',
            nome: 'required',
            mensagem: 'required',
            email: {
                required: true,
                email: true,
            },
        };

        const customMessages = {
            subject: 'Por favor, escolha uma opção.',
            Telefone: 'Campo obrigatório.',
            nome: 'Campo obrigatório.',
            mensagem : 'Campo obrigatório.',
            email: {
                required: 'Campo obrigatório',
                email: 'Por favor, informe um email válido.',
            },
        };
        contact.self.validate({
            ignore: ':hidden',
            rules: customRules,
            messages: customMessages,

            submitHandler(form,ev) {
                ev.preventDefault();
                const $form = $(form);               

                contact.loading.addClass('is--active');
                $form
                    .addClass('is--inactive');

                const data = {
                    email: contact.inputEmail.val(),
                    message: contact.inputMessage.val(),
                    nome: contact.inputName.val(),
                    subject: contact.inputSubject.val(),
                    telephone: contact.inputTelephone.val(),
                };

                self._sendForm(contact,data);

                return false
            },
        });
    },

    backToForm(contact) {
        contact.error.errorBtn.on('click', (ev) => {
            ev.preventDefault();

            contact.error.self.removeClass('is--active');
            contact.self
                .removeClass('is--inactive')
                .clearForm();
        });
    },

    _sendForm(contact,data) {
        console.log(data);
        VTEX.vtexMasterdata.insert({
            email: data.email, 
            message: data.message,
            nome:data.name,
            subject:data.subject,
            telephone:data.telephone},'CF')
            .then((res) => {
                console.log('Mensagem enviada com sucesso');
                contact.successMessage.addClass('is--active');
            })
            .fail((err) => { 
                console.log('Houve algum problema,tente novamente mais tarde');
                contact.error.self.addClass('is--active')
            })
            .always(() => {
                contact.loading.removeClass('is--active');

                if ( VTEX.isMobile ) {
                    $('html, body').animate({
                        scrollTop: contact.self.offset().top,
                    }, 500);
                }
            });

        return false;
    },
};