
import {
    pluralize,
    toTitleCase,
} from '../utils/helpers';

rivets.formatters.upper = (val) => val.toUpperCase();
rivets.formatters.prefix = (val, prefix) => prefix + val;
rivets.formatters.suffix = (val, suffix) => val + suffix;
rivets.formatters.capitalize = (val) => ( val.length > 2 ) ? VTEX.gh.capitalize(val) : val.toUpperCase();
rivets.formatters.slugifyText = (val) => VTEX.gh.slugifyText(val);
rivets.formatters.safeSum = (val, sum) => Math.round(((val * 1) + (sum * 1)) * 100) / 100;
rivets.formatters.lastIndex = (val, item) => val >= item.length - 1;
rivets.formatters.pluralize = (val, text) => `${val} ${( val > 1 ) ? pluralize(text) : text}`;
rivets.formatters.formatSizeText = (val, index) => val.split(' ')[index].replace(/[^a-zA-Z]+/g, '');
rivets.formatters.toTitleCase = (val) => toTitleCase(val);
rivets.formatters.toLowerCase = (val) => val.toLowerCase();
rivets.formatters.formatMinicartAmount = (val) => {
    const qtyGreaterThanOne = parseInt(val, 10) > 1;
    const suffix = qtyGreaterThanOne ? 'itens' : 'item';

    return `Subtotal <span>(${val} ${suffix})</span>`
};
rivets.formatters.formatShippingDays = (val, text = 'Até') => {
    const days = window.parseFloat(val, 2);

    return ( days > 1 ) ? `${text} ${days} dias úteis` : `${text} ${days} dia útil`;
};
