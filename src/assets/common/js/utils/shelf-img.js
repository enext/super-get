
export default {
    setImage(product, imageWidth, imageRatio, imageLabelName = false, hasCarousel = false) {
        let imageUrl = product.items[0].images[0].imageUrl;

        imageUrl = VTEX.vtexHelpers.getResizeImageByRatio(imageUrl, 'width', imageWidth, imageRatio);

        const setImageSrc = (imageUrl, hasCarousel) =>  ( !hasCarousel ) ? `data-src="${imageUrl}"` : `data-lazy="${imageUrl}"`;
        const setImageClass = (hasCarousel) => ( !hasCarousel ) ? `x-shelf__img has--lazy has--placeloader js--lazy` : `x-shelf__img has--placeloader`;

        let imageLabel = {};
        if ( imageLabelName ) {
            imageLabel = VTEX.globalHelpers.objectSearch(product, {imageLabel: imageLabelName});
            imageUrl = ( imageLabel.imageUrl ) ? imageLabel.imageUrl : imageUrl;
        }

        const Markup = `
            <img class="${setImageClass(hasCarousel)}"
                src="${VTEX.baseImage}"
                ${setImageSrc(imageUrl, hasCarousel)}
                alt="${product.productName}"
                title="${product.productName}" />`;

        VTEX.lazyload.update();

        return Markup;
    },
};
