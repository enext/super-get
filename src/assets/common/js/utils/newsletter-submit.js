
export default {
    newsletterValidate(newsletter) {
        const self = this;

        newsletter.form.validate({
            ignore: ':hidden',
            rules: {
                firstName: 'required',
                email: {
                    required: true,
                    email: true,
                },
                isNewsletterOptIn: 'required',
            },
            messages: {
                firstName: 'Campo obrigatório',
                email: {
                    required: 'Campo obrigatório',
                    email: 'Informe um e-mail válido',
                },
                isNewsletterOptIn: {
                    required: 'Por favor, aceite os termos',
                },
            },
            submitHandler(form) {
                const $form = $(form);
                const serializedFormFields = $form.serialize();
                const unserializedFormFields = VTEX.globalHelpers.unserialize(`?${serializedFormFields}`);

                newsletter.loading.addClass('is--active');
                $form
                    .add(newsletter.title)
                    .removeClass('is--active');
                self._sendForm(unserializedFormFields, newsletter);
            },
        });
    },

    backToForm(newsletter) {
        newsletter.errorBtn.on('click', (ev) => {
            ev.preventDefault();

            newsletter.error.removeClass('is--active');
            newsletter.form
                .add(newsletter.title)
                .addClass('is--active')
                .clearForm();
        });
    },

    _sendForm(fields, newsletter) {
        VTEX.vtexMasterdata.insertUpdateUser(fields.email, {firstName: fields.firstName, isNewsletterOptIn: true},'CL')
            .then((res) => {
                newsletter.success.addClass('is--active');
            })
            .fail((err) => newsletter.error.addClass('is--active'))
            .always(() => {
                newsletter.loading.add(newsletter.title).removeClass('is--active');
            });

        return false;
    },
};
