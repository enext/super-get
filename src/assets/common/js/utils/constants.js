export default {
    slickArrow: `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 41.68 41.68">
  <defs>
    <style>
      .cls-a {
        fill: #fff;
        stroke: #e8e8e8;
      }

      .cls-b {
        stroke: none;
      }

      .cls-c {
        fill: none;
      }

      .cls-d {
        filter: url(#Ellipse_7);
      }
    </style>
    <filter id="Ellipse_7" x="0" y="0" width="41.68" height="41.68" filterUnits="userSpaceOnUse">
      <feOffset dy="1" input="SourceAlpha"/>
      <feGaussianBlur stdDeviation="1.5" result="blur"/>
      <feFlood flood-opacity="0.161"/>
      <feComposite operator="in" in2="blur"/>
      <feComposite in="SourceGraphic"/>
    </filter>
  </defs>
  <g id="_" data-name="&gt;" transform="translate(-1206.16 -1297.16)">
    <g class="cls-d" transform="matrix(1, 0, 0, 1, 1206.16, 1297.16)">
      <g id="Ellipse_7-2" data-name="Ellipse 7" class="cls-a" transform="translate(4.5 3.5)">
        <circle class="cls-b" cx="16.34" cy="16.34" r="16.34"/>
        <circle class="cls-c" cx="16.34" cy="16.34" r="15.84"/>
      </g>
    </g>
    <path id="Path_155" data-name="Path 155" d="M3.462,956.875l-3.391,3.6a.25.25,0,0,0,0,.33.228.228,0,0,0,.33,0l3.768-4a.25.25,0,0,0,0-.33h0l-3.768-4a.228.228,0,0,0-.33,0,.228.228,0,0,0,0,.33l3.391,3.6s.188.188.188.235A2.049,2.049,0,0,1,3.462,956.875Z" transform="translate(1224.769 361.034)"/>
  </g>
</svg>`,

}
