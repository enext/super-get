export default {
    notifyMeFormValidate(notifyMeForm) {        
        const self = this;
        const customRules = {
            name: 'required',
            email: {
                required: true,
                email: true,
            },
        };

        const customMessages = {
            name: 'Campo obrigatório.',            
            email: {
                required: 'Campo obrigatório',
                email: 'Por favor, informe um email válido.',
            },
        };

        notifyMeForm.self.validate({
            ignore: ':hidden',
            rules: customRules,
            messages: customMessages,

            submitHandler(form,ev) {
                ev.preventDefault();
                const $form = $(form);

                const name = notifyMeForm.inputName.val();
                const email = notifyMeForm.inputEmail.val();
                const skuId = self._getSkuid();

                const data = {
                    'name':name,
                    'email': email,
                    'skuId':skuId,
                };
                
                
                notifyMeForm.loading.addClass('is--active');
                $form
                    .addClass('is--inactive');
                self._sendData(data, notifyMeForm);

                return false
            },
        });
    },

    _getSkuid() {
        return document.getElementById('notifyMeSubmit').dataset.sku
    },

    backToForm(notifyMeForm) {
        notifyMeForm.error.errorBtn.on('click', (ev) => {
            ev.preventDefault();

            notifyMeForm.error.self.removeClass('is--active');

            notifyMeForm.self
                .removeClass('is--inactive')
                .clearForm();
        });
    },

    _sendData(data, notifyMeForm) {
        VTEX.vtexHelpers.notifyMe(data.name, data.email, data.skuId)
            .then((res) => {
                
                notifyMeForm.successMessage.addClass('is--active');
            })
            .fail((err) => {
                
                notifyMeForm.error.addClass('is--active')
            })
            .always(() => {
                notifyMeForm.loading.removeClass('is--active');
            });        

        return false;
    },

    _getSkuJson(limit) {
        if(!skuJson) {
            for (var i = 0; i < limit; i++) {
                const propName = 'skuJson_' + i;
                if (window[propName]) {
                    return window[propName];
                }                
            }            
            return false
            
        } else {
            return skuJson
        }
    },
}