
import { map, filter, indexOf } from 'fast.js';

export const pluralize = (word) => {
    const rules = {
        add: {
            s: ['a', 'e', 'i', 'o', 'u', 'ã', 'ãe'],
            es: ['r', 'z', 'n', 'ás'],
            '': ['is', 'us', 'os'],
        },
    };

    const regex = '^([a-zA-Zà-úÀ-Ú]*)(%s)$';

    for ( const rule in rules ) {
        if ( {}.hasOwnProperty.call(rules, rule)) {
            switch ( rule ) {
                case 'add':
                    for ( const add in rules[rule] ) {
                        if ( {}.hasOwnProperty.call(rules[rule], add) ) {
                            const search = regex.replace('%s', rules[rule][add].join('|'));
                            const reg = new RegExp(search, 'i');

                            if ( reg.exec(word) !== null ) {
                                return word + add;
                            }
                        }
                    }

                default:
                    return false;
            }
        }
    }

    return word;
};

export const toTitleCase = (str) => {
    const articles = [
        // EN
        'a', 'an', 'the',
        // PT-BR
        'o', 'a', 'os', 'as', 'um', 'uma', 'uns', 'umas',
        'ao', 'aos', 'do', 'dos', 'no', 'nos', 'pelo', 'pelos', 'num', 'nuns',
        'à', 'às', 'da', 'das', 'na', 'nas', 'pela', 'pelas', 'numa',
    ];
    const conjunctions = [
        // EN
        'for', 'and', 'nor', 'but', 'or', 'yet', 'so',
        // PT-BR
        'e', 'ou', 'ora', 'quer', 'já', 'que', 'porque',
        'porquanto', 'pois', 'nem', 'mas', 'porém',
    ];
    const prepositions = [
        // EN
        'with', 'at', 'from', 'into', 'upon', 'of', 'to', 'in', 'for',
        'on', 'by', 'like', 'over', 'plus', 'but', 'up', 'down', 'off', 'near',
        // PT-BR
        'ante', 'após', 'até', 'com', 'contra', 'de', 'desde', 'em',
        'entre', 'para', 'perante', 'por', 'per', 'sem', 'sob', 'sobre', 'trás',
        'afora', 'como', 'conforme',
    ];

    // The list of spacial characters can be tweaked here
    // const replaceCharsWithSpace = (str) => str.replace(/[^0-9a-z&/\\]/gi, ' ').replace(/(\s\s+)/gi, ' ');
    const capitalizeFirstLetter = (str) => str.charAt(0).toUpperCase() + str.substr(1);
    const normalizeStr = (str) => str.toLowerCase().trim();
    const shouldCapitalize = (word, fullWordList, posWithinStr) => {
        if ( (posWithinStr == 0) || (posWithinStr == fullWordList.length - 1) ) {
            return true;
        }

        return !(articles.includes(word) || conjunctions.includes(word) || prepositions.includes(word));
    };

    // str = replaceCharsWithSpace(str);
    str = normalizeStr(str);

    let words = str.split(' ');
    if ( words.length <= 2 ) { // Strings less than 3 words long should always have first words capitalized
        words = map(words, (word) => capitalizeFirstLetter(word));
    } else {
        for ( let i = 0, len = words.length; i < len; i++ ) {
            words[i] = (shouldCapitalize(words[i], words, i) ? capitalizeFirstLetter(words[i], words, i) : words[i]);
        }
    }

    return words.join(' ');
};

export const formatSlas = (obj) => {
    if ( !VTEX.gh.isPlainObject(obj) ) {
        throw new Error('Response must be an Object');
    }

    const [{ slas }] = obj.logisticsInfo;
    const pickupStores = filter(slas, (sla) => VTEX.gh.contains('retira', sla.name.toLowerCase()));
    const shippings = filter(slas, (sla) => indexOf(pickupStores, sla) === -1);

    return {
        full: obj,
        shippings,
        pickupStores,
    };
};

export const entries = (object) => $.map(object, (value, key) => [[key, value]]);

export const getValueFromSearchPattern = (string = '', key) => [].concat(string.match((new RegExp(`${ key }=([^&]*)`))))[1];

export const objectsHaveDiferrentEntries = (object1, object2) => entries(object1).filter(([key, value]) => object2[key] != value).length > 0;

export const addIPSDataPromise = (orderform) => {
    const IPS = Cookies.get('IPS');
    const { marketingData } = orderform;
    const utmMedium = getValueFromSearchPattern(IPS, 'Midia');
    const utmCampaign = getValueFromSearchPattern(IPS, 'Campanha');
    const utmSource = getValueFromSearchPattern(IPS, 'Parceiro');

    return objectsHaveDiferrentEntries({ utmMedium, utmCampaign, utmSource }, (marketingData || {}))
        ? vtexjs.checkout.sendAttachment('marketingData', $.extend({}, marketingData, { utmCampaign, utmMedium, utmSource }))
        : $.Deferred((deferred) => deferred.resolve(orderform)).promise();
}
