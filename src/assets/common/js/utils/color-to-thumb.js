export default {
    transform ( color ) {
        const THUMB_PREFIX = 'thumb_filter_';
        const THUMB_FILE_EXT = '.jpg';

        if ( VTEX.globalHelpers.isString(color) ) {
            let slugColor = VTEX.globalHelpers.slugifyText(color);
            return THUMB_PREFIX + slugColor + THUMB_FILE_EXT;
        }
    }
};
