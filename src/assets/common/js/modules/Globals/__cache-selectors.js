
export default {
    minicart: {
        open: $('.js--minicart-open'),
        close: $('.js--minicart-close'),
        message: $('.js--minicart-message')
    },
    addToCart: {
        self: $('.js--add-to-cart'),
    },
    login: {
        btn: $('.js--login'),
        label: $('.js--login-label'),
    },
    search: {
        form: $('.js--search-form'),
        input: $('.js--search-input'),
        close: $('.js--search-close'),
        result: $('.js--search-result'),
        resultList: $('.js--search-result-list'),
        resultLabel: $('.js--search-result-label'),
        mobileHeaderForm: $('.js--header-menu-search-form'),
        mobileHeaderInput: $('.js--header-menu-search-input'),
    },
    
    header: {
        self: $('.js--header'),
    },

    shelf: {
        imgContainer: $('.js--shelf-img-container'),
    },

    mainMenu: {
        submenuWrapper: $('.js--main-menu-submenu-wrapper'),
        subMenuItems : $('.js--main-menu-items'),
    },

    footerNewsletter: {
        title: $('.js--footer-newsletter-title'),
        form: $('.js--footer-newsletter-form'),
        loading: $('.js--footer-newsletter-loading'),
        error: $('.js--footer-newsletter-error'),
        errorBtn: $('.js--footer-newsletter-error-btn'),
        success: $('.js--footer-newsletter-success'),
    },
    vtex: {
        helperComplement: $('.helperComplement'),
    },
};
