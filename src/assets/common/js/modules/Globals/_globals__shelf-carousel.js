
import ShelfImg from './../../utils/shelf-img.js';

const Methods = {
    init() {
        Methods.shelfCarousel();
        Methods.discountFlag();
    },

    discountFlag() {
        [...document.querySelectorAll('.js--shelf li')].map((li) => {
            let oldPrice = li.querySelector('.x-product__price--old strong');
            if (oldPrice) {
                oldPrice = parseFloat(oldPrice.innerHTML.replace(/\D/gi, ''));
                const bestPrice = parseFloat(li.querySelector('.x-product__price--best').innerHTML.replace(/\D/gi,''))
                const percentAmout = Math.floor(100 - bestPrice / oldPrice*100);
                li.querySelector('strong').textContent = percentAmout +'%';
            }
        });
    },

    shelfCarousel() {
        VTEX.shelfCarousel = new VTEX.VtexShelfProperties(VTEX.vtexUtils, VTEX.vtexCatalog, Methods.shelfCarouselProp, VTEX.setCache);
        VTEX.shelfCarousel.setEventName('shelfCarouselEnd');
        VTEX.shelfCarousel.setShelfContainer('.js--shelf-carousel');
    },

    shelfCarouselProp($el, product) {    
        const imageWidth = parseInt($el.data('imageWidth'));
        const imageRatio = parseFloat($el.data('imageRatio'));
        const markup = ShelfImg.setImage(product, imageWidth, imageRatio, false, true);

        $el.removeClass('has--placeloader');
        $el.empty().append(markup);
    },

};

export default {
    init: Methods.init,
};
