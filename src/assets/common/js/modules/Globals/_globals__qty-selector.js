
const Methods = {
    init() {
        Methods.qtyChange();
    },

    qtyChange() {
        $(document).on('click', '.js--qty-selector-btn', (ev) => {
            ev.preventDefault();

            const $this = $(ev.currentTarget);
            let oldValue = $this.parent().find('.js--qty-selector').val();
            let newVal = 1;

            if ( $this.data('qty-selector') === '+' ) {
                newVal = parseFloat(oldValue) + 1;
            } else {
                if ( oldValue > 1 ) {
                    newVal = parseInt(oldValue) - 1;
                } else {
                    return false;
                }
            }

            $this.parent().find('.js--qty-selector').val(newVal);
        });
    },
};

export default {
    init: Methods.init,
};
