
import VtexUtils from 'vtex-utils';
import VtexCatalog from 'vtex-catalog';
import VtexMasterdata from 'vtex-masterdata';
import VtexShelfProperties from 'vtex-shelf-properties';
import VtexWishlist from 'vtex-wishlist';
import RivetsUtilify from 'rivets-utilify';

const Methods = {
    init() {
        Methods.fontsLoad();
        Methods.setGlobals();
        Methods.setLazy();        
        Methods.closeMenus();
        Methods.closeCategoryMenus();
        Methods.shelvesFullUpdate();

        Methods.scrollActions();

        Methods.jQueryExtends();
        Methods.jQueryValidate();
    },

    fontsLoad() {
        const OpenSans300 = new FontFaceObserver('Open Sans', {weight: 300});
        const OpenSans400 = new FontFaceObserver('Open Sans', {weight: 400});
        const OpenSans600 = new FontFaceObserver('Open Sans', {weight: 600});
        const OpenSans700 = new FontFaceObserver('Open Sans', {weight: 700});

        Promise.all([
            OpenSans300.load(),
            OpenSans400.load(),
            OpenSans600.load(),
            OpenSans700.load(),
        ]);
    },

    setGlobals() {
        // Cache Elements
        VTEX.body             = $('body');
        VTEX.overlay          = $('.js--overlay');
        VTEX.menuOverlay      = $('.js--menu-overlay');
        VTEX.minicart         = $('.js--minicart');
        VTEX.searchBtnOpen    = $('.js--search-btn-open');
        VTEX.searchBtnClose   = $('.js--search-btn-close');
        VTEX.searchInput      = $('.js--search-input');
        VTEX.searchDrop       = $('.js--search-drop');
        VTEX.searchResultList = $('.js--search-result-list');
        VTEX.newsletter       = $('.js--newsletter');

        VTEX.menuMobile = $('.js--menu-mobile');

        // Product        
        VTEX.popupSizeChart = $('.js--product-size-chart');
        
        // Category
        VTEX.filtersContentMobile = $('.js--filters-content-mobile');

        // Category/Departament/Search templates
        VTEX.isHome = VTEX.body.hasClass('is--home');
        VTEX.isProduct = VTEX.body.hasClass('is--product');
        VTEX.isCategory = VTEX.body.hasClass('is--category')
            || VTEX.body.hasClass('is--departament')
            || VTEX.body.hasClass('is--search-result');
        VTEX.isSearchPage = VTEX.body.hasClass('is--search-result');

        // External Plugins
        VTEX.isMobile = isMobile.any;

        // Plugins
        VTEX.VtexShelfProperties = VtexShelfProperties;
        VTEX.VtexCatalog         = VtexCatalog;

        // Plugins Instances
        VTEX.vtexUtils      = new VtexUtils();
        VTEX.vtexUtils.version = '2.0.0';
        VTEX.vtexCatalog    = new VTEX.VtexCatalog(VTEX.vtexUtils);
        VTEX.vtexMasterdata = new VtexMasterdata(VTEX.vtexUtils);
        VTEX.vtexWishlist   = new VtexWishlist(VTEX.vtexUtils, VTEX.vtexMasterdata, VTEX.vtexCatalog);

        VTEX.vtexCatalog.setCamelize(true);
        VTEX.vtexCatalog.setPriceInfo(true);
        VTEX.vtexCatalog.setInstallmentsGroup(true);

        VTEX.vtexHelpers     = VTEX.vtexUtils.vtexHelpers;
        VTEX.globalHelpers   = VTEX.vtexUtils.globalHelpers;
        VTEX.locationHelpers = VTEX.vtexUtils.locationHelpers;

        // Set Plugins Options
        VTEX.vtexUtils.setRivetsUtilify(RivetsUtilify);
        VTEX.vtexCatalog.setCamelize(true);
        VTEX.vtexMasterdata.setStore('superget');

        VTEX.closeMenus = () => {
            $.publish('/VTEX/closeMenus');
        }
    },

    closeMenus() {
        VTEX.closeMenus = (removeOverlay) => {
            removeOverlay = ( VTEX.globalHelpers.isUndefined(removeOverlay) ) ?  false : removeOverlay;

            VTEX.minicart
                .add(VTEX.searchBtnClose)
                .add(VTEX.searchDrop)
                .removeClass('is--active');            

            VTEX.newsletter                
                .removeClass('is--active is--popup');


            // Header Main Menu
            VTEX.menuOverlay.removeClass('is--active');

            if ( isMobile.any ) {
                VTEX.menuMobile.removeClass('is--active');
                $('.js--menu-mobile-close').removeClass('is--active');
                $('.js--menu-mobile-btn').addClass('is--active');
            }
            
            

            // Search On Close Actions
            VTEX.searchInput.val('');
            VTEX.searchResultList.empty();
            VTEX.searchBtnOpen.addClass('is--active');

            if ( VTEX.isCategory ) {
                VTEX.closeCategoryAllDrops();
                
                if (VTEX.isMobile) {
                    VTEX.filtersContentMobile.removeClass('is--active');                    
                }
            }

            if ( removeOverlay ) {
                VTEX.overlay.removeClass('is--active');
                VTEX.body.removeClass('has--loader has--no-scroll has--search-loader has--minicart-loader has--wishlist-loader');
                VTEX.enableScroll();
            }            
        };
    },

    closeCategoryMenus() {
        // Category Filters
        VTEX.closeCategoryOrderby = () => {
            $('.js--order-text').removeClass('is--active');
            $('.js--order-drop').slideUp();
        };

        VTEX.closeCategoryFilters = () => {
            $('.js--toggle-filter').removeClass('is--active');
            $('.js--filters-content').slideUp();
        };

        VTEX.closeCategoryAllDrops = () => {
            VTEX.closeCategoryOrderby();
            VTEX.closeCategoryFilters();
        };
    },

    shelvesFullUpdate() {
        VTEX.shelfCategoryFullUpdate = () => {
            VTEX.shelfCategory.update();
            VTEX.lazyload.update();
        };
    },

    scrollActions() {
        const _preventDefault = (e) => {
            e = e || window.event;

            if ( e.preventDefault ) {
                e.preventDefault();
            }

            e.returnValue = false;
        };

        const _theMouseWheel = (e) => {
            _preventDefault(e);
        };

        const _disableScroll = () => {
            if ( window.addEventListener ) {
                window.addEventListener('DOMMouseScroll', _theMouseWheel, false);
            }

            window.onmousewheel = document.onmousewheel = _theMouseWheel;
        };

        const _enableScroll = () => {
            if (window.removeEventListener) {
                window.removeEventListener('DOMMouseScroll', _theMouseWheel, false);
            }

            window.onmousewheel = document.onmousewheel = null;
        };

        // Prevent Scroll
        VTEX.disableScroll = () => {
            if ( isMobile.any ) {
                VTEX.body.addClass('has--no-scroll');
            } else {
                _disableScroll();
            }
        };

        VTEX.enableScroll = () => {
            if ( isMobile.any ) {
                VTEX.body.removeClass('has--no-scroll');
            } else {
                _enableScroll();
            }
        };
    },

    setLazy() {
        VTEX.lazyload = new LazyLoad({
            data_src: 'src',
            data_srcset: 'srcset',
            class_loading: 'is--loading',
            class_loaded: 'is--loaded',
            class_error: 'has--lazy-error',
            elements_selector: '.js--lazy',
            threshold: 650,
            callback_set(self) {
                $(self).removeClass('has--lazy');
            },
            callback_load(self) {
                $(self).removeClass('has--placeloader');
            },
        });
    },

    /* eslint-disable */
    jQueryExtends() {
        $.fn.clearForm = function() {
            return this.each(function() {
                const type = this.type;
                const tag = this.tagName.toLowerCase();

                if ( tag === 'form' ) {
                    return $(':input', this).clearForm();
                }

                if ( type === 'text' || type === 'password' || tag === 'textarea' ) {
                    this.value = '';
                } else if ( type === 'checkbox' || type === 'radio') {
                    this.checked = false;
                } else if ( tag === 'select' ) {
                    this.selectedIndex = -1;
                }
            });
        };

        /**
         * Chunk an array of DOM Elements
         * @example
         *     // Will wrap every 3º items in a new div with 'item__new' class
         *     $('.item').chunk(3).wrap('<div class="item__new"></div>');
         */
        $.fn.chunk = function(size) {
            var arr = [];

            for ( var i = 0; i < this.length; i += size ) {
                arr.push(this.slice(i, i + size));
            }

            return this.pushStack(arr, 'chunk', size);
        };

        /* jQuery Tiny Pub/Sub - v0.7 - 10/27/2011
        * http://benalman.com/
        * Copyright (c) 2011 "Cowboy" Ben Alman; Licensed MIT, GPL */

        (function($) {

            var o = $({});
        
            $.subscribe = function() {
            o.on.apply(o, arguments);
            };
        
            $.unsubscribe = function() {
            o.off.apply(o, arguments);
            };
        
            $.publish = function() {
            o.trigger.apply(o, arguments);
            };
        
        }(jQuery));
    },

    jQueryValidate() {
        // Custom e-mail validate
        $.validator.methods.email = function(value, element) {
            return VTEX.globalHelpers.isEmail(value);
        };

        // Limit the number of files in a FileList.
        $.validator.addMethod('maxfiles', function( value, element, param ) {
            if ( this.optional(element) ) {
                return true;
            }

            if ( $(element).attr('type') === 'file' ) {
                if ( element.files && element.files.length > param ) {
                    return false;
                }
            }

            return true;
        }, $.validator.format('Please select no more than {0} files.'));

        // Limit the size of all files in a FileList.
        $.validator.addMethod('maxsizetotal', function(value, element, param) {
            if ( this.optional(element) ) {
                return true;
            }

            const len = element.files.length;

            if ( $(element).attr('type') === 'file' ) {
                if ( element.files && len ) {
                    let totalSize = 0;

                    for ( let i = 0; i < len; i += 1 ) {
                        totalSize += element.files[i].size;
                        if ( totalSize > param ) {
                            return false;
                        }
                    }
                }
            }

            return true;
        }, $.validator.format('Total size of all files must not exceed {0} bytes.'));

        /**
         * Limit the size of each individual file in a FileList.
         * @example
         *     rules: {
         *         file: {
         *             required: true,
         *             extension: 'txt|docx|pdf',
         *             maxsize: 5242880, // 1mb = 1048576
         *         },
         *     },
         *     messages: {
         *         file: {
         *             required: 'Campo obrigatório',
         *             extension: 'Arquivos devem ser TXT, DOCX ou PDF',
         *             maxsize: 'Tamanho máximo do arquivo 5mb',
         *         },
         *     },
         */
        $.validator.addMethod('maxsize', function(value, element, param) {
            if ( this.optional(element) ) {
                return true;
            }

            const len = element.files.length;

            if ( $(element).attr('type') === 'file' ) {
                if ( element.files && len ) {
                    for ( let i = 0; i < len; i += 1 ) {
                        if ( element.files[i].size > param ) {
                            return false;
                        }
                    }
                }
            }

            return true;
        }, $.validator.format('File size must not exceed {0} bytes each.'));
    },
    /* eslint-enable */
};

export default {
    init: Methods.init,
};
