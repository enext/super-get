
import './../../utils/outline.js';
import { fromPolyfill } from './../../vendor/polyfills.js';
import { isNaNPolyfill } from './../../vendor/polyfills.js';

import GlobalsInit from './_globals__init.js';
import GlobalsVtex from './_globals__vtex.js';

import GlobalsShelfCarousel from './_globals__shelf-carousel.js';
import GlobalsShelfCategory from './_globals__shelf-category.js';

import GlobalsQtySelector from './_globals__qty-selector.js';
import GlobalsAtendimento from './_globals__atendimento.js';

import GlobalsSearchActions from './_globals__search-actions.js';
import GlobalsSearch from './_globals__search.js';
import GlobalsLogin from './_globals__login.js';
import GlobalShelfBuy from './_globals__shelf-buy.js';

import GlobalsOverlay from './_globals__overlay.js';
import GlobalsMinicart from './_globals__minicart.js';
import GlobalsWishlist from './_globals__wishlist.js';
import GlobalsFooterNewsletter from './_globals__footer-newsletter.js';
import GlobalsFooterMenu from './_globals__footer-menu.js';
import GlobalsPopUpPrivacy from './_globals_popup-privacy';

const init = () => {
    fromPolyfill();
    isNaNPolyfill();
    GlobalsInit.init();
    GlobalsVtex.init();

    GlobalsShelfCarousel.init();
    GlobalsShelfCategory.init();
    GlobalShelfBuy.init();
    GlobalsAtendimento.init();

    setTimeout(() => {
        GlobalsQtySelector.init();
    }, 100);

    setTimeout(() => {
        GlobalsSearchActions.init();
        GlobalsSearch.init();
        GlobalsLogin.init();
    }, 150);

    setTimeout(() => {
        GlobalsOverlay.init();
        GlobalsMinicart.init();
        GlobalsWishlist.init();
        GlobalsFooterNewsletter.init();
        GlobalsFooterMenu.init();
        GlobalsPopUpPrivacy.init();
    }, 200);
};

export default {
    init: init,
};
