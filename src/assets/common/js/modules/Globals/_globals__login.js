
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.login;
const Methods = {
    init() {
        Methods.setMenuLogin();
        Methods.setAjaxMenuLogin();
        Methods.doLogin();
    },

    /**
     * Recursive AJAX Login
     */
    setAjaxMenuLogin() {
        $(window).on('closed.vtexid', (ev) => {
            Methods.setMenuLogin();
        });
    },

    setMenuLogin() {
        VTEX.vtexHelpers.checkLogin().then((user) => {
            if ( user.IsUserDefined ) {
                El.btn.parent().addClass('is--logged');
            }
        });
    },

    doLogin() {
        El.btn.on('click', (ev) => {
            ev.preventDefault();
            const $this = $(ev.currentTarget);
            const $parent = $this.parent();

            if ( $parent.hasClass('is--logged') ) {
                window.location.href = '/account';

                return false;
            }

            VTEX.closeMenus();
            VTEX.body.addClass('has--loader');
            VTEX.vtexHelpers.checkLogin().fail((user) => {
                if ( ! user.IsUserDefined ) {
                    VTEX.vtexHelpers.openPopupLogin(true);
                    return false;
                }
            });
        });
    },
};

export default {
    init: Methods.init,
};
