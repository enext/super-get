
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.vtex;
const Methods = {
    init() {
        Methods.vtexIdEvents();
        Methods.closeLoginModal();
        Methods.removeHelperComplement();
        Methods.abrirModals();

        $(document).on('ajaxStop', () => Methods.removeHelperComplement);
    },

    removeHelperComplement() {
        El.helperComplement.remove();
    },

    closeLoginModal() { 
        $(document).on('click', '.vtexIdUI-close', (ev) => {            
            VTEX.closeMenus(true)
            const isLogin = $('body').hasClass('is--login');
            if (isLogin) {
                window.location.href = '/';
            }
        });
    },

    vtexIdEvents() {
        $(window).on('started.vtexid', (ev) => {
            VTEX.body.addClass('has--loader');
            VTEX.overlay.addClass('is--active');
        });

        $(window).on('closed.vtexid', (ev) => VTEX.closeMenus(true));
        $(window).on('rendered.vtexid', (ev) => VTEX.body.removeClass('has--loader'));
    },
    abrirModals() {
        document.querySelector(".x-header #quero-alugar").addEventListener("click", function(event){
            
            if($('body').hasClass('is--home')) {
                event.preventDefault();
            } 

            const sair = document.querySelector(".x__modal__agendamento__container__sair");
            const modal = document.querySelector(".x__modal__agendamento");
            const carregamento = document.querySelector(".x__modal__agendamento__container__carregamento__estado");
            carregamento.classList.remove("x-modal--is--loading");

            modal.classList.add("x-modal--is--open");
            modal.classList.remove("x-modal--is-not-open");

            if(modal.classList.contains("x-modal--is--open")){
                document.querySelector("body").classList.add("x-block--scroll");
                setTimeout(function(){
                    carregamento.classList.add("x-modal--is--loading");
                }, 3000);

                sair.addEventListener("click", function(){
                    carregamento.classList.remove("x-modal--is--loading");
                    modal.classList.remove("x-modal--is--open");
                    document.querySelector("body").classList.remove("x-block--scroll");
                    modal.classList.add("x-modal--is-not-open");
                })

                setTimeout(function(){
                    if(carregamento.offsetWidth === 707){
                        window.location.href = "https://site.getnet.com.br/maquininha/aluguel/";
                    }
                }, 6000)
            }
        });
    },
};



export default {
    init: Methods.init,
};
