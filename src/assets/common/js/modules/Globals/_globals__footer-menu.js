import { accordionAction } from './../../utils/accordion.js';

const Methods = {
    init() {
        Methods.setFooterAccordion();
    },

    setFooterAccordion() {
        if (!VTEX.isMobile) {
            $(document).on('click', '.js--footer-accordion-title', (ev) => {
                const $this = $(ev.currentTarget);
                const $item = $('.js--footer-item');
                const $title = $('.js--footer-accordion-title');            
                const $content = $('.js--footer-accordion-content');
                
                $item.toggleClass('is--active');
                $content.toggleClass('is--active');
            });

        } else {
            $(document).on('click', `.js--footer-accordion-title`, (ev) => {   
                ev.preventDefault();
                ev.stopPropagation();
                const $this = $(ev.currentTarget);                
                const $accordionContent = $this.next();
                const $siblings = $this.parent().siblings();
                const $accordionsContent = $siblings.find(`.js--footer-accordion-content`);
                const $accordionsTitle = $siblings.find(`.js--footer-accordion-title`);                
                $accordionsTitle.removeClass('is--active');
                $this.toggleClass('is--active');

                $accordionsContent
                    .removeClass('is-active')
                    .slideUp('fast')

                $accordionContent.slideToggle('fast');

            });            
        }
    }
};

export default {
    init: Methods.init,
};
