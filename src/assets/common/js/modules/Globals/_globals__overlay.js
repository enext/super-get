
const Methods = {
    init() {
        Methods.closeOverlay();
    },

    closeOverlay() {
        VTEX.overlay.on('click', (ev) => {
            VTEX.closeMenus(true);
        });

        $(document).on('closeMenus.VTEX', '.js--overlay', (ev) => {
            $(ev.currentTarget).removeClass('is--active');
        });
    },
};

export default {
    init: Methods.init,
};
