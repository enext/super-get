
import CacheSelectors from './__cache-selectors';
import NewsletterSubmit from './../../utils/newsletter-submit.js';

const El = CacheSelectors.newsletter;
const Methods = {
    init() {
        Methods.openSideNewsletter();
        Methods.closePopupNewsletter();
        Methods.setSubmit();
    },

    openSideNewsletter() {
        VTEX.sideNewsletterBarContainer.on('click', (ev) => {
            const $this = $(ev.currentTarget);

            if ( VTEX.sideNewsletterBarBtn.hasClass('is--active') ) {
                VTEX.closeMenus(true);
                VTEX.sideNewsletterBarContainer.removeClass('is--active');
                return false;
            }

            VTEX.closeMenus();
            VTEX.disableScroll();

            // $this
            VTEX.sideNewsletterBarBtn
                .add(VTEX.overlay)
                .add(VTEX.newsletter)
                .add(VTEX.sideNewsletterBarContainer)
                .addClass('is--active');
        });
    },

    closePopupNewsletter() {
        El.close.on('click', (ev) => {
            VTEX.closeMenus();
        });
    },

    setSubmit() {
        NewsletterSubmit.newsletterValidate(El);
        NewsletterSubmit.backToForm(El);
    },
};

export default {
    init: Methods.init,
};
