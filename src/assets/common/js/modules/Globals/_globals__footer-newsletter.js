
import CacheSelectors from './__cache-selectors';
import NewsletterSubmit from './../../utils/newsletter-submit.js';

const El = CacheSelectors.footerNewsletter;
const Methods = {
    init() {
        Methods.setSubmit();
    },

    setSubmit() {
        NewsletterSubmit.newsletterValidate(El);
        NewsletterSubmit.backToForm(El);
    },
};

export default {
    init: Methods.init,
};
