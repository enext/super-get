
const Methods = {
    init() {
        Methods.instafeedRun();
    },

    instafeedRun() {  
        const feed = new Instafeed({
            get: 'user',
            target: 'instafeedContainer',
            accessToken: '231834475.9a32152.2dbe458f16f342d1b070f4ba0f900586',
            userId: '231834475',
            clientId: '9a321523085b4e8fa39a84b661b6a4b3',
            limit: ( VTEX.isMobile ) ? 6 : 12,
            resolution: 'low_resolution',
            orientation: 'square',
            template: `
                <div class="x-instafeed__item js--instafeed-item">
                    <a class="x-instafeed__link" href="{{link}}" title="{{caption}}" target="_blank" rel="nofollow, noreferrer">
                        <img class="x-instafeed__img has--placeloader" src="${VTEX.baseImage}" data-lazy="{{image}}" alt="{{caption}}" title="{{caption}}" width="306" height="306" /> </a>
                </div>
            `,
            after() {                
                const AUTOPLAY_SPEED = ( VTEX.isMobile ) ? 1000 : 0;
                const SPEED = 2000;                
                $('#instafeedContainer').slick({
                    slidesToShow: ( VTEX.isMobile ) ? 2 : 5,
                    autoplay: true,
                    autoplaySpeed: AUTOPLAY_SPEED,
                    speed: SPEED,
                    centerMode: ( VTEX.isMobile ) ? false : true,
                    infinite: true,   
                    arrows: false,
                    fade: false,
                    cssEase: 'linear',
                    lazyLoad: ( VTEX.isMobile ) ? VTEX.slickLazyMobile : VTEX.slickLazyDesktop,                
                })

                .on('lazyLoaded', (event, slick, image, imageSource) => {
                    $(image).removeClass('has--placeloader');
                });
            },
        });

        feed.run();
    },

    instafeedSlider() {

    },
};

export default {
    init: Methods.init,
};




