
import ShelfImg from './../../utils/shelf-img.js';

const Methods = {
    init() {
        Methods.shelfCategory();
        Methods.discountFlag();
    },

    shelfCategory() {
        VTEX.shelfCategory = new VTEX.VtexShelfProperties(VTEX.vtexUtils, VTEX.vtexCatalog, Methods.shelfCategoryProp, VTEX.setCache);
        // VTEX.shelfCategory.setEventTime(VTEX.eventTime);
        VTEX.shelfCategory.setEventName('shelfCategoryEnd');
        VTEX.shelfCategory.setShelfContainer('.js--shelf-category');
    },

    shelfCategoryProp($el, product) {
        const imageWidth = parseInt($el.data('imageWidth'));
        const imageRatio = parseFloat($el.data('imageRatio'));
        const markup = ShelfImg.setImage(product, imageWidth, imageRatio, false, false);
        $el.removeClass('has--placeloader');
        $el.empty().append(markup);
    },
    discountFlag() {
        [...document.querySelectorAll('.js--shelf li')].map((li) => {
            let oldPrice = li.querySelector('.x-product__price--old strong');
            if (oldPrice) {
                oldPrice = parseFloat(oldPrice.innerHTML.replace(/\D/gi, ''));
                const bestPrice = parseFloat(li.querySelector('.x-product__price--best').innerHTML.replace(/\D/gi, ''));
                const percentAmout = Math.floor(100 - bestPrice / oldPrice*100);
                li.querySelector('strong').textContent = percentAmout +'%';
            }
        });
    },
};

export default {
    init: Methods.init,
};
