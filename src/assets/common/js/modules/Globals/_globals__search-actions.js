
import CacheSelectors from './__cache-selectors.js';

const El = CacheSelectors.search;
const Methods = {
    init() {
        Methods.openSearch();
        Methods.closeSearch();
        Methods.sendForm();
    },

    openSearch() {
        VTEX.searchBtnOpen.on('click', (ev) => {
            const $this = $(ev.currentTarget);

            VTEX.closeMenus();
            VTEX.body.addClass('has--no-scroll')
            VTEX.searchInput.focus();
            $this.removeClass('is--active');
            VTEX.searchBtnClose
                .add(VTEX.searchDrop)
                .add(VTEX.menuOverlay)
                .addClass('is--active');
            
                
            
        });
    },

    closeSearch() {
        VTEX.searchBtnClose.on('click', (ev) => {
            VTEX.closeMenus(true);
        });
    },

    sendForm() {
        El.form.on('submit', (ev) => {
            const term = El.input.val().replace(/\./g, '');

            window.location.href = `/${term}?O=OrderByReleaseDateDESC`;
            return false;
        });

        $(document).on('click', '[data-search-show-more]', (ev) => El.form.submit());

        // Mobile Header
        if ( isMobile.any ) {
            El.mobileHeaderForm.on('submit', (ev) => {
                const term = El.mobileHeaderInput.val();

                window.location.href = `/${term}?O=OrderByReleaseDateDESC`;
                return false;
            });
        }
    },
};

export default {
    init: Methods.init,
};
