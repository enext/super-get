
const Methods = {
    init() {
        Methods.openInfo();
    },
    openInfo(){
        $('.showInfo').on("click", function() {
	        if($('.x-superget-sell__item').hasClass('is--active')){
    	        $('.x-superget-sell__item').removeClass('is--active')
	        };
            $(this).parents('.x-superget-sell__item').toggleClass('is--active');
        });
        $('.infoText .close').on("click", function(){
            $(this).parents('.x-superget-sell__item').removeClass('is--active');
        });
    },
};

export default {
    init: Methods.init,
};
