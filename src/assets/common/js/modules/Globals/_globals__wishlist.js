
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.login;
const Methods = {
    init() {
        Methods.initWishlist();
    },

    initWishlist() {
        const options = {
            storeName: 'superget', // Nome da url da loja (required)
            shelfId: '30ac7018-1aaa-4abf-aa4e-462142286de2', // Id da vitrine que renderizará os elementos (required)
            orderBy: 'OrderByPriceASC', // (default)
            zeroPadding: true, // (default: false)
            reloadPage: false, // (default: true)

            linkTitle: {
                add: 'Adicionar a wishlist',
                remove: 'Remover da wishlist',
            },
            notFound() { return '<div class="x-wishlist__not-found">Nenhum produto em sua lista</div>'; }, // Element to append if no results (default)

            perPage: 12,

            // classes
            activeClass: 'is--active',
            emptyClass: 'is--wishlist-empty',
            loaderClass: 'has--wishlist-loader',
            itemsClass: 'x-wishlist__items',
            itemClass: 'x-wishlist__item',

            orderByBodyClass: 'has--wishlist-order-by',

            loadMoreBodyClass: 'has--wishlist-load-more',
            loadMoreWrapperClass: 'x-wishlist__load-more-wrapper',
            loadMoreBtnClass: 'x-wishlist__load-more-btn',
            loadMoreText: 'Carregar mais',
        };

        VTEX.vtexWishlist.setOptions(options);

        if ( VTEX.body.hasClass('is--wishlist-page') ) {
            VTEX.vtexWishlist.renderProducts();
            $(document).on('afterShowItems.vtexWishlist', (ev) => VTEX.shelfCategoryFullUpdate());
        }

        $(document).on('requestEnd.vtexSearchAutocomplete', (ev) => VTEX.vtexWishlist.update());
    },
};

export default {
    init: Methods.init,
};
