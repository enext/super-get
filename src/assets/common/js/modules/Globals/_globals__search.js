
import './../../vendor/vtex-search-autocomplete.js';

import CacheSelectors from './__cache-selectors.js';

const El = CacheSelectors.search;
const Methods = {
    init() {
        Methods.autocomplete();
    },

    autocomplete() {
        const $paragraph = $('<p />', {
            class: 'x-search__not-found-text',
            text: 'Não encontramos nenhum resultado',
        });
        const $notFound = $('<div />', {
            class: 'x-search__not-found',
            append: $paragraph,
        });

        El.input.vtexSearchAutocomplete({
            shelfId: '32fba543-8034-46e2-a9dc-2468cb4bd4ab', // SuperGetCommon-ShelfSearch
            appendTo: El.resultList,
            limit: !VTEX.isMobile ? 8 : 4,
            bodyClass: 'has--search-loader',
            notFound() {
                return $notFound;
            },
        });
    },
};

export default {
    init: Methods.init,
};
