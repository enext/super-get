const Methods = {
  init() {
    Methods.setPopupPrivacy();
  },
  setPopupPrivacy() {
    const privacyAllowed = Boolean(Methods.getCookie("PrivacyAndPolicy"));

    if (!privacyAllowed) {
      $(".js--privacy-policy").removeClass("is--hide");
      setTimeout(() => {
        $(".js--privacy-policy").fadeIn();
      }, 300);
    }

    $(document).on("click", ".js--privacy-cta", () => {
      Methods.setCookie("PrivacyAndPolicy", "true", "365");
        $(".js--privacy-policy").fadeOut();
      });
  },
  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  },
  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  },
};

  export default {
    init: Methods.init,
  };
