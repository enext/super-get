
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.minicart;
const Methods = {
    init() {
        Methods.minicart();
        Methods.actions();
        Methods.events();
    },

    minicart() {
        VTEX.minicart.vtexMinicart({
            vtexUtils: VTEX.vtexUtils,
            vtexCatalog: VTEX.VtexCatalog,
            debug: true,
            cache: VTEX.setCache,
            zeroPadding: true,
            camelizeItems: true,
            camelizeProps: [''],
            bodyClass: 'has--loader',
        });
    },

    actions() {
        El.open.on('click', (ev) => {
            ev.preventDefault();

            VTEX.closeMenus();
            Methods._openCart();
        });

        El.close.on('click', (ev) => {
            ev.preventDefault();
            VTEX.closeMenus(true);
        });

        // El.logedVerify.on('click', (ev) => {
        //     if(!vtexjs.checkout.orderForm.loggedIn){
        //         ev.preventDefault();
        //         vtexid.start({
        //             returnUrl: '/checkout',
        //         })

        //     }
        // });
    },

    events() {
        $(document).on('delete.vtexMinicart', (ev, orderForm) => {
            if (orderForm.items.length < 1) {
                VTEX.closeMenus(true);
            }
        });
    },

    _openCart() {
        VTEX.minicart
            .add(VTEX.overlay)
            .addClass('is--active');
    },
};

export default {
    init: Methods.init,
};
