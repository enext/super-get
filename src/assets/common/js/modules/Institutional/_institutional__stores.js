
import CacheSelectors from './__cache-selectors.js';

const El = CacheSelectors.institutional;
const Methods = {
    init() {
        Methods.main();
    },

    main() {
        const isStore = $('body').hasClass('is--store');

        if (isStore) {            
            VTEX.map = new GMaps({
                div: '#map', 
                lat: -19.9380558,
                lng: -43.9455882,
            });

            VTEX.map.addMarker({
                lat: -19.9380558,
                lng: -43.9455882,
                title: 'Renata Campos',              
            }); 

               
        }
    },
};

export default {
    init: Methods.init,
};
