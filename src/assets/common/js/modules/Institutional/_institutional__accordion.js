import Accordion from "./../../utils/accordion.js";

const Methods = {
    init() {
        Methods.setAccordionAction();
    },

    setAccordionAction() {
        $(document).on("click", `.js--vve-accordion-title`, ev => {
            const $this = $(ev.currentTarget);
            const $accordionContent = $this.next();
            const $siblings = $this.parent().siblings();
            const $accordionsContent = $siblings.find(
                `.js--vve-accordion-content`
            );
            const $accordionsTitle = $siblings.find(`.js--vve-accordion-title`);
            $accordionsTitle.removeClass("is--active");
            $this.toggleClass("is--active");

            $accordionsContent.slideUp();
            $accordionContent.slideToggle();
        });
    }
};

export default {
    init: Methods.init
};
