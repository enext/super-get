export default {
    institutional: {
        menu: {
            items: $(".js--institutional-menu-items"),
            item: $(".js--institutional-menu-item"),
            link: $(".js--institutional-menu-link")
        }
    },

    contactForm: {
        self: $(".js--contact-form"),
        inputName: $(".js--contact-name"),
        inputTelephone: $(".js--contact-telephone"),
        inputEmail: $(".js--contact-email"),
        inputSubject: $(".js--contact-subject"),
        inputMessage: $(".js--contact-message"),
        submitBtn: $(".js--contact-submit"),
        loading: $(".js--contact-loading"),
        error: {
            self: $(".js--contact-error"),
            errorBtn: $(".js--contact-error-btn"),
            errorMessage: $(".js--contact-error")
        },
        successMessage: $(".js--contact-success")
    }
};
