import CacheSelectors from "./__cache-selectors.js";
import SidebarMenu from "./../../utils/sidebar-menu.js";

const El = CacheSelectors.institutional.menu;
const Methods = {
    init() {
        Methods.setMenuActions();
    },

    setMenuActions() {
        SidebarMenu.activeMenu(El);

        if (isMobile.any) {
            SidebarMenu.mobileArccordion(El, "institutional");
        }
    }
};

export default {
    init: Methods.init
};
