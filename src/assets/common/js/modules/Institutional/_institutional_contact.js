import Contact from './../../../../common/js/utils/contact-submit.js';
import CacheSelectors from './__cache-selectors';

const El = CacheSelectors.contactForm;
const Methods = {
    init() {
        Methods.main();
    },

    main() {
        Contact.contactFormValidate(El);
        Contact.backToForm(El);
    },
};

export default {
    init: Methods.init,
};
