import InstitutionalMain from "./_institutional__main.js";
import InstitutionalMenu from "./_institutional__menu.js";
import InstitutionalAccordion from "./_institutional__accordion.js";
import InstitutionalContact from "./_institutional_contact.js";
import InstitutionalStores from "./_institutional__stores.js";

const init = () => {
    InstitutionalMain.init();
    InstitutionalMenu.init();
    InstitutionalAccordion.init();
    InstitutionalContact.init();
    InstitutionalStores.init();
};

export default {
    init: init
};
