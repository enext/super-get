
import methods from './methods/index';
import models from './models/index';

export default {
    init() {        
        methods.init();
        models.init();
    },
};
