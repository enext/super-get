export default {
    init() {
        moreInfo();
    }
}

function moreInfo() {
    var desc = $('.x-superget-sell__contents');
    var descTextSeo = $('.x-superget-sell__contents_description');

    // show more button if desc too long
    if (desc.height() > descTextSeo.height()) {
        $('.more-info').show();
    }

    $('.more-info').click(function() {
            $('.x-superget-sell__center').toggleClass('expand')
            $(this).toggleClass('expand');
    });
}