
import methods from './methods/index';
import models from './models/index';
import taxas from './taxas-e-tarifas/index';
import moreInfo from './more-info/index';

export default {
    init() {        
        methods.init();
        models.init();
        taxas.init();
        moreInfo.init();
    },
};
