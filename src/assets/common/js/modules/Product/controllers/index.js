
import { cart } from './cart';
import { flags } from './flags';

export const controller = {        
    cart,
    flags,
};
