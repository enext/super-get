
import { assign } from 'fast.js';
import { props } from '../data/props';

const propsClone = assign({}, props);

export const flags = {
    select(ev, scope) {
        const { flags, props, state, product: { skuInfo } } = scope.app;        
        const flag = ev.currentTarget.dataset['flag'].toLowerCase();
        flags.chosen = flag;
        flags.hasError = false;
        state.buyBtnError = false;

        $('.x-product__flags-list li').removeClass('is--active');
        $(ev.currentTarget).addClass('is--active');
    }
};


