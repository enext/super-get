
import { assign } from 'fast.js';
import { props } from '../data/props';
import { addIPSDataPromise } from '../../../utils/helpers';
const propsClone = assign({}, props);

export const cart = {
    add(ev, scope) {
        const { flags, props, state, product: { skuInfo } } = scope.app;

        if (!flags.chosen) {
            flags.hasError = true;
            state.buyBtnError = true;
            
            return false
        }

        const item = [{
            id: skuInfo.itemId,
            quantity: props.qty,
            seller: '1',            
        }];

        const attachmentName = 'Chip';
        const content = { Chip: flags.chosen };

        props.buyBtnName = 'Adicionando';
        state.isFetchingCart = true;

        function getOrderFormPromise() {
            return vtexjs.checkout.getOrderForm()
        }

        function addToCartPromise(product) {
            return vtexjs.checkout.addToCart(item, null, 1)
        }

        // takes a addTocart orderform and return the addAttachment's promise
        function  handleAddToCartPromise(orderForm,item) {
            const newItemPosition =  orderForm.items.reduce(($newItemPosition, $item, $index) => {
                try {
                     return $item.id == item[0].id && $item.attachments.length === 0
                     && $item.id !== 4
                          ? $index
                          : $newItemPosition;
                } catch (error) {
                     return $newItemPosition;
                }
                
           }
           , false);

           if(newItemPosition){
                return vtexjs.checkout.addItemAttachment(newItemPosition, attachmentName, content, null, false);
           }
        }
        function canSendProduct(_orderForm, _product) {

            const productQtd = _orderForm.items.reduce(($qtd, $item) => {

                 if ($item.id == item[0].id) {
                      return $qtd + $item.quantity;
                 }
  
                 return $qtd;
  
            }, 0);

            if (productQtd >= 3) {
                 return false;
            }
  
            return true;
       }
        // first orderForm Get
        getOrderFormPromise()
            .done(orderForm => {
                const filteredById = orderForm.items.filter((currentItem, index) => {                                     
                    return currentItem.id === item[0].id;
                });

                // doesn't have the product on cart yet
                // if (!filteredById.length) {
                    
                if (!canSendProduct(orderForm, item)) {
                    alert(`Você só pode ter no máximo 3 itens deste produto no carrinho.\nPara comprar mais de 3 unidades do mesmo equipamento ligue para 4002-4000.`);
                    props.buyBtnName = propsClone.buyBtnName;
                    state.isFetchingCart = false;
                }
                else if(orderForm.items.length !== 0){
                    const productsEquals = [];
                    orderForm.items.forEach(($item, $index) => {
     
                         //Id do produto 4 que não tem bandeira
                         if ($item.id == 4 && item[0].id == 4) {
     
                              productsEquals.push({
                                   product: $item,
                                   index: $index
                              });
     
                         }
                         else if ($item.id === item[0].id && $item.attachments[0].content["Chip"] == content["Chip"]) {
     
                              productsEquals.push({
                                   product: $item,
                                   index: $index
                              });
                         }
     
                    });

                    if (productsEquals.length > 0 && productsEquals[0]) {
                        const updateItem = {
                             index: productsEquals[0].index,
                             quantity: productsEquals[0].product.quantity + 1
                        };
                        vtexjs.checkout.updateItems([updateItem], null, false)
                        .then(addIPSDataPromise)
                        .then((orderform) => {
                            window.location.href = window.jscheckoutUrl;
                        });
                   } else{
 
                       addToCartPromise(item)
                       .then(addIPSDataPromise)
                       .done(orderForm => {
                           handleAddToCartPromise(orderForm,item)
                               .done((orderForm) => {
                                   props.buyBtnName = propsClone.buyBtnName;
                                   state.isFetchingCart = false;
                                   window.location.href = window.jscheckoutUrl;
                                })
    
                               .fail(err => console.log(err))
                       })
    
                       .fail(err => console.log(err))
                   }

                }else{
                    vtexjs.checkout.addToCart(item)
                    .then(addIPSDataPromise)
                    .done(function (orderForm) {
                        vtexjs.checkout.addItemAttachment(0, attachmentName, content, null, false);
                        props.buyBtnName = propsClone.buyBtnName;
                        state.isFetchingCart = false;
                        window.location.href = window.jscheckoutUrl;
                   });
                }
                   

                // already has the product
                // } else {
                                                            
                // }
            })
            .fail(err => console.log(err))
            .always(() => console.log('always'));        
    },    
};


