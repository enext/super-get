
export default {
    init() {
        getProduct();
    },
};

function getProduct() {
    const sku = VTEX.vtexHelpers.stringIdsToArray(window.vtxctx.skus, ';')[0];   
     
    VTEX.vtexCatalog.searchSku(sku)
        .then((res) => $(document).trigger('FETCHING_PRODUCT_SUCCESS', [res]))
        .fail((err) => $(document).trigger('FETCHING_PRODUCT_FAILURE', [err]));
}
