import {
    product as El
} from './../../../modules/Globals/selectors';

import {
    data
} from '../data/index';
import {
    controller
} from '../controllers/index';

export default {
    init() {
        bindData()
            .then(() => {
                APP.productComponent.app.state.isFetchingProduct = false;
                $(document).trigger('FETCHING_PRODUCT_DATABIND');
            });
    },
};

function bindData() {
    return $.Deferred((def) => {
        $(document).on('FETCHING_PRODUCT_SUCCESS', (ev, product) => {
            data.product = product;
            data.product.skuInfo = data.product.items[0];
            //data.product.skuInfo.available = false;
            data.props.buyBtnName = data.product.skuInfo.available ? 'Comprar' : 'Indisponível';
            
            $('.x-superget-sell__contents').html(data.product.description);
            data.product.navImages = data.product.skuInfo.images;  
            data.product.images = data.product.skuInfo.images;

            if (data.product.skuInfo) {
                const discount = Math.floor(100 - (data.product.skuInfo.bestPrice / data.product.skuInfo.listPrice * 100));
                data.product.discountPercentage = discount;
            }

            const chip = data.product.chip;
            
            if( chip ) {
                chip.unshift(chip.pop());
            }
            
            APP.productComponent = rivets.bind(El.$productComponent, {
                // spread obj
                app: {
                    product : data.product,
                    state: data.state,
                    props: data.props, 
                    flags: data.flags,                   
                    controller,
                },
            }).models;

            def.resolve();
        });
    }).promise();
}  

function discountPercentage(listPrice, bestPrice) {
    const discount = VTEX.vh.getPercentage(listPrice, bestPrice);    
}
