
import bindData from './bind-data';
import getProduct from './get-product';
// import description from './description';

export default {
    init() {
        bindData.init();
        getProduct.init();
        // description.init();
    },
};
