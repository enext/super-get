
import VanillaAccordion from '../../../../../common/js/vendor/vanilla-accordion';

export default {
    init() {
        descAccordion();
    },
};

function descAccordion() {
    VTEX.productDescAccordion = new VanillaAccordion({
        elements: '.js--desc-wrapper',
        trigger: '.js--desc-trigger',
        content: '.js--desc-content',
        oneAtATime: false,
    });
}
