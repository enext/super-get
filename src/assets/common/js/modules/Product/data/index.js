
import { props } from './props';
import { state } from './state';
import { flags } from './flags';

export const data = {
    props,
    state,
    flags,
    product: [],    
};
