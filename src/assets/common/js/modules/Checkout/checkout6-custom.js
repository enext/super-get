const Methods = {
  init() {
    Methods.criaCampo();
    Methods.mascaraAniversario();
    Methods.validaMaiorDeIdade();
    Methods.criaCampoMensagem();
    Methods.criaCampoPrivacyPolicy();
    Methods.sendMasterDataInformation();
    Methods.addMessage();
    // Methods._setDisabled();
    // Methods.hashChangeActions();
    Methods._setFakeButton();
    Methods._fakeButtonAction();
    Methods.setCheckboxInit();
    Methods.getAge();
    Methods.btnActionGetAge();
    Methods.mensagemBoleto();
    Methods.createPopupForRegions();
    Methods.setStateEvents();
    Methods.setShippingNumberValidation();
    // Methods.complementAction();
  },

  setShippingNumberValidation() {
    $(window).on('hashchange', () => {
      Methods.createPostalCodeValidation();
      Methods.createSpanErrorComplementAddress();
      Methods.createValidationComplementAddress();
    });
    $(document).ajaxStop(() => {
      Methods.createPostalCodeValidation();
      Methods.createValidationComplementAddress();
    })
  },

  createPostalCodeValidation() {
    const { hash } = window.location;
    if (!hash.includes('/shipping')) {
      return;
    };

    $(window['ship-number']).keyup(function(event) {
      const goToPayment = $(window['btn-go-to-payment']);
      const residentialNumber = $(this);
      const regex = new RegExp(/[0-9]/gm);
      const key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

      if (!residentialNumber.val() || (!regex.test(key) && event.which != '8')) {
        goToPayment.addClass('x-hide');
        residentialNumber.addClass('error');
        residentialNumber.removeClass('success');
        return;
      }

      residentialNumber.addClass('success');
      residentialNumber.removeClass('error');
    });

    const $shipNumberEl = $(window['ship-number']);
    const shipNumberValue = +$shipNumberEl.val();
    const mustHideGoToPaymentBtn = !!$shipNumberEl.length && (isNaN(shipNumberValue) || shipNumberValue === 0);
    mustHideGoToPaymentBtn && $(window['btn-go-to-payment']).addClass('x-hide');
  },

  complementAction() {
    $(window).on('click', '#btn-go-to-payment', function(ev) {
      console.log('click');
      const $complementInput = $('#ship-complement');

      if ($complementInput.length && !!$complementInput.val()) {
        ev.preventDefault();
        alert('Por favor, insira o complemento');
      }
    });
  },

  createSpanErrorComplementAddress() {
    $('p.ship-complement').append(`<span class="help error x-hide">Campo obrigatório.</span>`);
  },

  createValidationComplementAddress() {
    const { hash } = window.location;
    if (!hash.includes('/shipping')) {
      return;
    };

    $(window['ship-complement']).attr('placeholder', 'Complemento');

    $(window['ship-complement']).keyup(function(event) {
      const goToPayment = $(window['btn-go-to-payment']);
      const $complementInput = $(event.currentTarget);

      if (!$complementInput.val().length) {
        goToPayment.addClass('x-hide');
        $complementInput.addClass('error');
        $complementInput.removeClass('success');
        $('p.ship-complement span.error').removeClass('x-hide');
        return;
      };

      $complementInput.addClass('success');
      $complementInput.removeClass('error');
      $('p.ship-complement span.error').addClass('x-hide');
      goToPayment.removeClass('x-hide');
    });
  },

  toggleCloseRegionPopup() {
    $('.x-chip__popup').addClass('hide');
    $('.x-chip__popup__container').addClass('hide');
    $('body').removeClass('hide-overflow');
    sessionStorage.setItem('js--region--popup', true);
  },

  createPopupForRegions() {
    if (!$('.x-chip__popup').length) {
      const popupLayerHTML = '<div class="x-chip__popup hide"></div>';
      const popupContainerHtml = '<div class="x-chip__popup__container hide">\
                                    <button class="x-chip__popup__container__close js--close-region">X</button>\
                                    <h2 class="x-chip__popup__container__title">Chip Multi</h2>\
                                    <div class="x-chip__popup__container__paragraph">\
                                      <p class="inner__text">\
                                        Afim de melhorarmos a disponibilidade do serviço para a sua região, enviaremos um chip multi operadora.\
                                      </p>\
                                      <button class="inner__close js--continue-region">Continuar</button>\
                                    </div>\
                                  </div>';
      $('body').append(popupLayerHTML);
      $('body').append(popupContainerHtml);


      $(document).on('click', '.js--close-region , .js--continue-region , .x-chip__popup', () => {
        Methods.toggleCloseRegionPopup();
      });

      if ($('.x-chip__popup').hasClass('hide')) {
        $(window).on('keyup', function(evt) {
          if (evt.keyCode === 27) {
            Methods.toggleCloseRegionPopup();
          }
        })
      };
    }
  },

  setStateEvents() {
    const $popup = $('.x-chip__popup');

    function showRegionNotification() {
      /*if (!sessionStorage.getItem('js--region--popup')) {
        $popup.removeClass('hide');
        $('.x-chip__popup__container').removeClass('hide');
        $('body').addClass('hide-overflow');
      }*/
    };

    $(window).on('orderFormUpdated.vtex', () => {
      const findState = {
        'PI': () => showRegionNotification(),
        'ES': () => showRegionNotification(),
        'CE': () => showRegionNotification(),
        'MA': () => showRegionNotification(),
        'RS': () => showRegionNotification(),
        'AC': () => showRegionNotification(),
        'MT': () => showRegionNotification(),
        'RR': () => showRegionNotification(),
        'TO': () => showRegionNotification(),
        'RN': () => showRegionNotification(),
        'PR': () => showRegionNotification(),
        'PO': () => showRegionNotification(),
        'fora-da-regra': _ => {
          return $popup.addClass('hide') &&
            $('body').removeClass('hide-overflow') &&
            $('.x-chip__popup__container').addClass('hide');
        }
      }

      const UF = vtexjs.checkout.orderForm.shippingData &&
        vtexjs.checkout.orderForm.shippingData.address ?
        vtexjs.checkout.orderForm.shippingData.address.state :
        "fora-da-regra" || 'fora-da-regra';
      if (!UF) {
        return;
      }
      (findState[UF.toUpperCase()] || findState['fora-da-regra'])();
    })
  },

  criaCampo() {
    // polyfill
    (function(arr) {
      arr.forEach(function(item) {
        if (item.hasOwnProperty('append')) {
          return;
        }
        Object.defineProperty(item, 'append', {
          configurable: true,
          enumerable: true,
          writable: true,
          value: function append() {
            var argArr = Array.prototype.slice.call(arguments),
              docFrag = document.createDocumentFragment();

            argArr.forEach(function(argItem) {
              var isNode = argItem instanceof Node;
              docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
            });

            this.appendChild(docFrag);
          }
        });
      });
    })([Element.prototype, Document.prototype, DocumentFragment.prototype]);

    var pContainer = document.createElement("p");
    pContainer.setAttribute('class', 'client-phone input pull-left text required dateclass');
    document.querySelector('.box-client-info-pf').appendChild(pContainer);

    var inputDate = document.createElement('input');
    inputDate.setAttribute('type', 'text');
    inputDate.setAttribute('placeholder', 'xx/xx/xxxx');
    inputDate.setAttribute('id', 'client-birth')
    inputDate.setAttribute('class', 'input-small error')
    inputDate.setAttribute('maxlength', '10')

    var labelDate = document.createElement('label');
    labelDate.append('Data de nascimento');
    labelDate.setAttribute('for', 'client-birth');

    pContainer.appendChild(labelDate);
    pContainer.appendChild(inputDate);

    var spanError = document.createElement('span');
    spanError.setAttribute('class', 'help error')
    pContainer.append(spanError);

    inputDate.addEventListener('focusout', function() {
      if (inputDate.value == '') {
        spanError.textContent = 'Campo obrigatório';
      } else if (inputDate.value != '') {
        spanError.textContent = ''
      }
    })
  },

  criaCampoMensagem() {
    var pMessage = document.createElement("p");
    pMessage.setAttribute('class', 'message');

    var labelMessage = document.createElement("label");
    labelMessage.setAttribute('class', 'checkbox message-label');

    var inputMessage = document.createElement('input');
    inputMessage.setAttribute('type', 'checkbox');
    inputMessage.setAttribute('id', 'opt-in-mensagem');
    // inputMessage.setAttribute("checked", "checked");

    var spanMessage = document.createElement('span');
    spanMessage.setAttribute('class', 'message-text');
    spanMessage.append("Aceito receber mensagens por whatsapp");

    //Tornando Filho...
    document.querySelector('.box-client-info').appendChild(pMessage);
    document.querySelector('.message').appendChild(labelMessage);
    document.querySelector('.message-label').appendChild(inputMessage);
    document.querySelector('.message-label').appendChild(spanMessage);
  },

  criaCampoPrivacyPolicy() {
    var pMessage = document.createElement("p");
    pMessage.setAttribute('class', 'privacy');

    var labelMessage = document.createElement("label");
    labelMessage.setAttribute('class', 'checkbox privacy-label');

    var inputMessage = document.createElement('input');
    inputMessage.setAttribute('type', 'checkbox');
    inputMessage.setAttribute('required', '');
    inputMessage.setAttribute('id', 'opt-in-privacyPolicy');
    // inputMessage.setAttribute("checked", "checked");

    var spanMessage = document.createElement('span');
    spanMessage.setAttribute('class', 'message-text');
    spanMessage.setAttribute('style', 'text-indent: 42px;');
    spanMessage.style.textIndent = '42px';
    $(spanMessage).append("Concordo com a politica de <a href='https://site.getnet.com.br/politica-de-privacidade/' target='_blank'> privacidade </a> e <a href='https://site.getnet.com.br/politica-de-privacidade/' target='_blank' style='margin-left:36px'> termos de usos </a>");

    //Tornando Filho...
    document.querySelector('.box-client-info').appendChild(pMessage);
    document.querySelector('.privacy').appendChild(labelMessage);
    document.querySelector('.privacy-label').appendChild(inputMessage);
    document.querySelector('.privacy-label').appendChild(spanMessage);
  },

  setCheckboxInit() {
    document.querySelector('#opt-in-newsletter').checked = false
    document.querySelector('#opt-in-mensagem').checked = false;
    document.querySelector('#opt-in-privacyPolicy').checked = false;
  },
  //Funcao criada para chamar a função getAge quando aparecer para digitar o email, pois o email é salvo no local storage para poder puxar a data na API 14/11
  btnActionGetAge() {
    $('#btn-client-pre-email').click(function() {
      Methods.getAge();
    })
  },
  getAge() {
    // var email = JSON.parse(localStorage.getItem('_vw_attributes')).userEmail;

    setTimeout(function() {
      var email = '';
      var preEmail = document.querySelector('#client-pre-email').value;

      if (preEmail != null) {
        email = preEmail;
      }
      localStorage.setItem("email_user", email);

      console.log(email);
      $.ajax({
        type: 'GET',
        url: '/api/dataentities/CL/search?_where=email=' + email + '&_fields=birthDate',
        success: function(res) {
          if (res[0].birthDate) {
            var dataCheia = res[0].birthDate.replace('T00:00:00', '').split('-');
            var aux;
            aux = dataCheia[0];
            dataCheia[0] = dataCheia[2];
            dataCheia[2] = aux;
            var dataAniversario = dataCheia.join('/');
            var input = document.querySelector('#client-birth');
            input.value = dataAniversario;
            console.log(input.value);
            window.hasValidAge = true;

          } else {
            console.log('Sem idade registrada');
          }
        }
      })
    }, 5000);

  },

  mascaraAniversario() {
    var input = document.getElementById('client-birth');

    var dateInputMask = function dateInputMask(elm) {
      elm.addEventListener('keypress', function(e) {
        if (e.keyCode < 47 || e.keyCode > 57) {
          e.preventDefault();
        }

        var len = elm.value.length;

        // If we're at a particular place, let the user type the slash
        // i.e., 12/12/1212
        if (len !== 1 || len !== 3) {
          if (e.keyCode == 47) {
            e.preventDefault();
          }
        }

        // If they don't add the slash, do it for them...
        if (len === 2) {
          elm.value += '/';
        }

        // If they don't add the slash, do it for them...
        if (len === 5) {
          elm.value += '/';
        }
      });
    };
    dateInputMask(input)
  },

  _getAge(birthDateString) {
    // yyyy/mm/dd
    var dateParsed = Date.parse(birthDateString);

    var birthDay = new Date(dateParsed);


    var today = new Date();
    var age = today.getFullYear() - birthDay.getFullYear();


    if (today.getMonth() < birthDay.getMonth()) {
      age--;
    }

    if (today.getMonth() == birthDay.getMonth() && today.getDate() < birthDay.getDate()) {
      age--;
    }

    return age
  },

  _setDisabled() {
    document.getElementById('go-to-shipping').disabled = true;
    document.getElementById('go-to-payment').disabled = true;
  },

  hashChangeActions() {

    window.addEventListener('hashchange', function(ev) {
      if (location.hash === '#/profile') {
        Methods._setDisabled();
      }
    }, false);

  },

  validaMaiorDeIdade() {
    var events = ['blur'];
    var nasc = document.querySelector('#client-birth');
    events.map(event => {
      nasc.addEventListener(event, function() {
        var inputAniversario = document.getElementById("client-birth").value.split('/').reverse().join('/');
        var age = Methods._getAge(inputAniversario);

        if (isNaN(age)) {
          alert('Data inválida,por favor insira uma data válida no formato dd/mm/aaaa');
          window.hasValidAge = false;
          return false
        }

        if (age < 18) {
          console.log('lt');
          window.hasValidAge = false;
          alert('Compra permitida apenas para maiores de 18 anos');
          // if (document.getElementById('go-to-shipping').length > 0) {
          //     console.log('shipping');
          //     document.getElementById('go-to-shipping').disabled = true;
          // }
          // if (document.getElementById('go-to-shipping')) {
          //     console.log('payment');
          //     document.getElementById('go-to-shipping').disabled = true;

          // }
        } else {
          console.log('gt');
          window.hasValidAge = true;
          // if (document.getElementById('go-to-shipping')) {
          //     console.log('shipping');
          //     document.getElementById('go-to-shipping').disabled = false;
          // }

          // if (document.getElementById('go-to-shipping')) {
          //     console.log('payment');
          //     document.getElementById('go-to-shipping').disabled = false;
          // }
        }
      })
    })
  },

  sendMasterDataInformation() {

    var btnPmnt = document.querySelector('#go-to-payment');

    btnPmnt.addEventListener('mouseover', function() {
      if (!document.querySelector('#client-birth').value) {
        alert('Informe a data de nascimento para prosseguir')
        Methods._setDisabled();
      }
    })
    document.querySelector('#client-birth').addEventListener('blur', function() {
      $('#go-to-payment').removeAttr('disabled');
    })

    btnPmnt.addEventListener('click', function(e) {
      let privacyOptin = document.querySelector('#opt-in-privacyPolicy').checked;
      console.log(privacyOptin);

      if (!privacyOptin) {
        alert('Você deve aceitar os termos de privacidade e uso!')
        return;
      }
      var headers = {
        "Accept": "application/vnd.vtex.ds.v10+json",
        "Content-Type": "application/json"
      }

      var nascSplit = document.getElementById("client-birth").value.split("/").reverse().join('-');


      if (document.querySelector('#client-email')) {
        var informations = {
          email: document.querySelector('#client-email').value,
          firstName: document.querySelector('#client-first-name').value,
          // lastName: document.querySelector('#client-last-name').value,
          homePhone: document.querySelector('#client-phone').value,
          document: document.querySelector('#client-document').value,
          isWhatsAppOptIn: document.querySelector('#opt-in-mensagem').checked,
          isPrivatePolicyOptin: document.querySelector('#opt-in-privacyPolicy').checked,
          birthDate: nascSplit

        };
        // $.ajax({
        //     type: "PUT",
        //     url: '/api/dataentities/CL/documents',
        //     headers: headers,
        //     data: JSON.stringify(informations),
        //     //                 processData: false,
        //     dataType: 'json',
        //     success: function (data) {
        //         console.log('Dados enviados', informations);
        //         console.log("Sucesso: ", data)
        //     },
        //     error: function (err) {
        //         console.log("Erro: ", err);
        //     }
        // });
      } else {
        var informations = {
          email: document.querySelector('.client-email.text.required > span').textContent,
          firstName: document.querySelector('#client-first-name').value,
          // lastName: document.querySelector('#client-last-name').value,
          homePhone: document.querySelector('#client-phone').value,
          document: document.querySelector('#client-document').value,
          isWhatsAppOptIn: document.querySelector('#opt-in-mensagem').checked,
          birthDate: nascSplit

        };
        // $.ajax({
        //     type: "PUT",
        //     url: '/api/dataentities/CL/documents',
        //     headers: headers,
        //     data: JSON.stringify(informations),
        //     //                 processData: false,
        //     dataType: 'json',
        //     success: function (data) {
        //         console.log('Dados enviados', informations);
        //         console.log("Sucesso: ", data)
        //     },
        //     error: function (err) {
        //         console.log("Erro: ", err);
        //     }
        // });
      }

    });
    var btn = document.querySelector('#go-to-shipping');
    btn.addEventListener('mouseover', function() {
      if (!document.querySelector('#client-birth').value) {
        alert('Informe a data de nascimento para prosseguir')
        Methods._setDisabled();
      } else {
        $('#go-to-shipping').removeAttr('disabled');
      }
    })

    document.querySelector('#client-birth').addEventListener('blur', function() {
      $('#go-to-shipping').removeAttr('disabled');
    })

    btn.addEventListener('click', function(e) {

      let privacyOptin = document.querySelector('#opt-in-privacyPolicy').checked;

      if (!privacyOptin) {
        alert('Você deve aceitar os termos de privacidade e uso!');
        return;
      }

      if (document.querySelector('#client-birth').value) {

        var headers = {
          "Accept": "application/vnd.vtex.ds.v10+json",
          "Content-Type": "application/json"
        }

        var nascSplit = document.getElementById("client-birth").value.split("/").reverse().join('-');

        if (document.querySelector('#client-email')) {
          var informations = {
            email: document.querySelector('#client-email').value,
            firstName: document.querySelector('#client-first-name').value,
            // lastName: document.querySelector('#client-last-name').value,
            homePhone: document.querySelector('#client-phone').value,
            document: document.querySelector('#client-document').value,
            isWhatsAppOptIn: document.querySelector('#opt-in-mensagem').checked,
            isPrivatePolicyOptin: document.querySelector('#opt-in-privacyPolicy').checked,
            birthDate: nascSplit

          };
          // $.ajax({
          //     type: "PUT",
          //     url: '/api/dataentities/CL/documents',
          //     headers: headers,
          //     data: JSON.stringify(informations),
          //     //                 processData: false,
          //     dataType: 'json',
          //     success: function (data) {
          //         console.log('Dados enviados', informations);
          //         console.log("Sucesso: ", data)
          //     },
          //     error: function (err) {
          //         console.log("Erro: ", err);
          //     }
          // });
        } else {
          var informations = {
            email: document.querySelector('.client-email.text.required > span').textContent,
            firstName: document.querySelector('#client-first-name').value,
            // lastName: document.querySelector('#client-last-name').value,
            homePhone: document.querySelector('#client-phone').value,
            document: document.querySelector('#client-document').value,
            isWhatsAppOptIn: document.querySelector('#opt-in-mensagem').checked,
            isPrivatePolicyOptin: document.querySelector('#opt-in-privacyPolicy').checked,
            birthDate: nascSplit

          };
          // $.ajax({
          //     type: "PUT",
          //     url: '/api/dataentities/CL/documents',
          //     headers: headers,
          //     data: JSON.stringify(informations),
          //     //                 processData: false,
          //     dataType: 'json',
          //     success: function (data) {
          //         console.log('Dados enviados', informations);
          //         console.log("Sucesso: ", data)
          //     },
          //     error: function (err) {
          //         console.log("Erro: ", err);
          //     }
          // });
        }

      } else {
        var editProfileBtn = document.getElementById('edit-profile-data');
        var birthdayInput = document.getElementById('client-birth');
        editProfileBtn.click();
        birthdayInput.focus();

        if (true) {
          setTimeout(function() {
            $('html, body').animate({
              scrollTop: $('#client-birth').offset().top - 24,
            }, 225);
          }, 225)
        }
      }
    });
  },

  addMessage() {
    $(document).ajaxSuccess(function(event, xhr, options, data) {
      const isUpdate = options.url.indexOf('update') !== -1;

      if (isUpdate) {
        const strToMatch = /Você só pode ter no máximo/;
        const hasMatched = data.messages[0].text.match(strToMatch);

        if (hasMatched) {
          const el = document.getElementsByClassName('vtex-front-messages-detail')[0];
          el.textContent = el.textContent + '.Para comprar mais de 3 unidades do mesmo equipamento ligue para 4002-4000.';
        }
      }
    });
  },

  _setFakeButton: function() {
    var cartEnd = document.getElementById('payment-data-submit');
    var fakeButton = Object.assign(document.createElement('div'), {
      className: 'x-checkout__fake-button',
      id: 'ghostButton',
    });

    cartEnd.parentNode.insertBefore(fakeButton, cartEnd.nextSibling);
  },

  _fakeButtonAction: function() {
    var fakeButton = document.getElementById('ghostButton');

    fakeButton.addEventListener('click', function(ev) {
      var hasValidAge = window.hasValidAge;
      if (hasValidAge) {

        var $submitBtn = $('.payment-submit-wrap button');

        $submitBtn.each(function(index) {
          var $this = $(this);
          var text = $this.text().trim();

          if (text === 'Finalizar compra') {
            $this.click();
            fakeButton.remove();
          }
        });

      } else {
        var editProfileBtn = document.getElementById('edit-profile-data');
        var birthdayInput = document.getElementById('client-birth');
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        alert('Data de nascimento inválida ou não informada, por favor, verifique e tente novamente');
        editProfileBtn.click();
        birthdayInput.focus();

        if (true) {
          setTimeout(function() {
            $('html, body').animate({
              scrollTop: $('#client-birth').offset().top - 24,
            }, 225);
          }, 225)
        }
      }
    });
  },

  mensagemBoleto() {
    $(window).on('load hashchange', (event) => {
      console.log("$('.payment-description'): ", $('.payment-description').length);
      $('.payment-description:not(:first)').remove();
      $('.payment-description').replaceWith(`
                <p class="payment-description">O banco pode levar até 3 dias úteis para confirmar o pagamento do boleto. Lembre-se que o prazo de entrega só começa a valer depois desta confirmação.</p>
                <p class="payment-description">Então, fica a dica! Se quiser receber sua maquininha mais rápido e começar a vender, sugerimos o pagamento com Cartão de Crédito.</p>
                <p class="payment-description">Caso ainda prefira por boleto, clique em "fechar pedido" , em seguida copie a linha digitável e pague pelo app do seu banco. Pague sem sair de casa! Fique atento à data de vencimento para sua compra não ser cancelada.</p>
            `);
    });
  },
}

function removeCartBar() {
  if (window.location.hash !== '#/cart'){
    $('.x-checkout-bar').hide();
    $('.transactions.span5.pull-right>span').hide();
  } else {
    $('.x-checkout-bar').show();
    $('.transactions.span5.pull-right>span').show();
  }
};

function addExceto() {
  let excetoTag = $('.transactions.span5.pull-right')
  if (excetoTag.children().length === 1) {
    excetoTag.append('<span>*Exceto para Superget Mobile</span>')
  }
}


document.addEventListener('DOMContentLoaded', function() {
  Methods.init();
});


function numericKeyboard() {

  $('#client-birth').prop('type', 'tel');
  $('#ship-number').prop('type', 'tel');
  $('#ship-number').attr('maxlength', '10');
  $('#ship-complement').attr('maxlength', '40');

};

$(window).on('hashchange', function() {
  removeCartBar()
});


$(window).on('orderFormUpdated.vtex', function(evt, orderForm) {
  addExceto();
});


$(document).on('keyup input blur focus', '#ship-number', (event) => {
  const { currentTarget: input } = event;
  setTimeout(() => {
    $(input).attr('value', $(input).val().replace(/\D/g, ''));
  }, 0);
});

document.addEventListener('DOMContentLoaded', numericKeyboard());
setInterval(function() { numericKeyboard(); }, 1000);