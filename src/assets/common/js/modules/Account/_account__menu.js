
import CacheSelectors from './__cache-selectors.js';
import SidebarMenu from './../../utils/sidebar-menu.js';

const El = CacheSelectors.account.menu;
const Methods = {
    init() {
        Methods.setMenuActions();
        Methods.moveOrderStatus();
    },

    setMenuActions() {
        SidebarMenu.activeMenu(El);

        if ( isMobile.any ) {
            SidebarMenu.mobileArccordion(El, 'account');
        }
    },

    moveOrderStatus() {
        setTimeout(function() {
            if( $('.x-account__orders .myo-view-order>section').length ) {
                $('.x-account__orders .myo-progress-bar').insertBefore( $('.x-account__orders .myo-view-order>section') );
            }
        },2000);
    },
};

export default {
    init: Methods.init,
};
