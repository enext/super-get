
import CacheSelectors from './__cache-selectors.js';

const El = CacheSelectors.account;
const Methods = {
    init() {
        // Account
        Methods.changeEditProfileLinkPosition();
        Methods.changeNewAddressLinkPosition();
        Methods.addLogoutBtn();

        Methods.accountActions();
        Methods.scrollTopForm();

        Methods.radioGenderStyle();

        // Orders
        Methods.changeOrdersImg();
        
        Methods.ajustWidthProdutc();
        Methods.removeCancelBtn();
        
        $(window).on('hashchange', Methods.shippmentInfo());
    },

    removeCancelBtn(){
        $('.myo-cancel-btn').remove()
    },

    // Account
    accountActions() {
        // Close all Modals
        $('.close, .btn-link').on('click', (ev) => {
            ev.preventDefault();
            $('.modal').hide();
        });

        // Edit user profile
        $('#edit-data-link').on('click', (ev) => {
            ev.preventDefault();

            Methods._dataBusiness();
            $('#editar-perfil-conteudo #email').attr('disabled', 'disabled');
            $('#editar-perfil').show();
        });

        // delete user address
        $('.delete').on('click', (ev) => {
            ev.preventDefault();
            $('#address-remove').show();
        });

        // Update user address
        $('.address-update').on('click', (ev) => {
            ev.preventDefault();
            $('#address-edit').show();
        });
    },

    addLogoutBtn() {
        const $menuheader = $('.x-account-menu__items'); //Desktop
        // const $profileDetailDisplay = $('.profile-detail-display'); // Mobile
        const btnMarkup = `
            <a class="x-account__logout-btn" href="/no-cache/user/logout" title="Sair">SAIR</a>`;

        if ( isMobile.any ) {
            // $profileDetailDisplay.prepend(btnMarkup);
            $menuheader.append(btnMarkup);
        } else {
            $menuheader.append(btnMarkup);
        }
    },

    ajustWidthProdutc() {
        setInterval(() => {
            let imgProduct = $('.myo-order-product img');

            for (var i = 0; i < imgProduct.length; i++) {
                let newUrl = $(imgProduct[i]).attr('src').replace('50-50', '200-200')
                $(imgProduct[i]).attr('src', newUrl);
            }
        }, 100);
        

    },


    changeEditProfileLinkPosition() {
        const $profileDetailDisplay = $('.profile-detail-display');
        const $editProfileLink = $('.edit-profile-link');

        $profileDetailDisplay.append($editProfileLink);
    },

    changeNewAddressLinkPosition() {
        const $addressDisplayBlock = $('.address-display-block');
        const $newAddressLink = $('.new-address-link');

        $addressDisplayBlock.append($newAddressLink);
    },

    scrollTopForm() {
        const $editDataLink = $('#edit-data-link');
        const $addressUpdate = $('.address-update');
        const headerTotalHeigh = 72;
        const scrollTop = (ev) => {
            ev.preventDefault();

            $('html, body').animate({
                scrollTop: $('.js--account-menu-items').offset().top - headerTotalHeigh,
            }, 750);
        };

        if ( isMobile.any ) {
            $editDataLink.on('click', scrollTop);
            $addressUpdate.on('click', scrollTop);
        }
    },

    radioGenderStyle() {
        const $input = $('input[name=gender]');
        const selectEl = (el) => {
            const $el = $(el);
            const $parent = $el.parent();

            $parent
                .add($parent.siblings())
                .removeClass('is--active');

            if ( $el.prop('checked') ) {
                $parent.addClass('is--active');
            }
        }

        $input.map((i, item) => selectEl(item));
        $input.on('click', (ev) => selectEl(ev.currentTarget));
    },

    // Orders
    changeOrdersImg() {
        const $btnContainer = $('#my-orders-container nav');
        const resizeImg = (context = false) => {
            const self = ( ! context ) ? $('#my-orders-container') : context;

            self.find('img').map((index, item) => {
                const $this = $(item);
                const imgSrc = $this.attr('src');
                if ( VTEX.globalHelpers.contains('/arquivos', imgSrc) ) {
                    $this.attr('src', '');
                    // $this.attr('src', VTEX.vtexHelpers.getResizedImage(imgSrc, 79, 111));
                    $this.attr('src', VTEX.vtexHelpers.getResizedImage(imgSrc, 68, 95));
                }
            });
        }

        $(document).on('ajaxStop', () => resizeImg());
        $(document).on('click', '#my-orders-container', (ev) => {
            const $this = $(ev.currentTarget);

            setTimeout(() => resizeImg($this), 100);
        });
    },

    shippmentInfo() {
        const changeText = () => {
            const $el = $('#my-orders-container');
            const $titleShipping = $el.find('h2.f4.mb0.lh-copy')
            const $container = $titleShipping.parent();
            const $shippingContainer = $titleShipping.next('p.mb0');
            const shippingText = $shippingContainer.find('span.bg-light-blue').text();

            const markup = `
                <p class="x-orders__info-shipping">Se você escolheu a opção de envio por transportadora, rastreie seu pedido por <a href="https://tracking.totalexpress.com.br/tracking/0?cpf_cnpj=" target="_blank" style="text-decoration: underline;">aqui</a>.</p>`;

            if ( shippingText.toLowerCase() === 'transportadora' ) {
                $container.append(markup);
            }

            // Change table head title
            const $tableTh = $container.find('table thead tr th:first-child span');
            $tableTh.text('Produto');
        };

        $(document).on('ajaxStop', () => changeText());
        $(window).on('hashchange', (ev) => setTimeout(() => changeText(), 200));
    },

    /**
     * @access private
     * Fix text on data business
     */
    _dataBusiness() {
        const $businessToggle = $('#business-toggle');
        const insertDataText = 'Incluir dados de pessoa jurídica';
        const removeDataText = 'Não usar dados de pessoa jurídica';

        $businessToggle.text(insertDataText);

        $businessToggle.on('click', (ev) => {
            const $this = $(ev.currentTarget);
            const data = $this.attr('data');

            if ( data === 'on' ) {
                $businessToggle.text(removeDataText);

                return false;
            }

            $businessToggle.text(insertDataText);
        });
    },
};

export default {
    init: Methods.init,
};
