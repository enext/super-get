
const Methods = {
    init() {
        Methods.changeGrid();
        Methods.orderByActions();
        Methods.closeOrderByOnClick();
    },

    changeGrid() {
        $('.js--change-grid').on('click', (ev) => {
            const $this = $(ev.currentTarget);
            const col = $this.data('value') || 3;

            $('.js--change-grid').removeClass('is--active');
            $this.addClass('is--active');

            $('[data-wishlist-items]').attr('data-col', col);
        });
    },

    orderByActions() {
        $('.js--order-text').on('click', (ev) => {
            const $this = $(ev.currentTarget);

            VTEX.closeCategoryFilters();
            $this.toggleClass('is--active');
            $('.js--order-drop').slideToggle();
        });
    },

    closeOrderByOnClick() {
        $('[data-wishlist-order-by]').on('click', 'label', (ev) => {
            VTEX.closeCategoryOrderby();
        });
    },
};

export default {
    init: Methods.init,
};
