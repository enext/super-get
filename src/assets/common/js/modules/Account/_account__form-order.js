import CacheSelectors from './__cache-selectors.js';

const El = CacheSelectors.account.menu;
const Methods = {
    init() {
        Methods.userFormOrder();
        Methods.addressFormOrder();
        Methods.validaMaiorDeIdade();
        Methods.validateUserCelPhone();
        $(window).load(Methods.appendCelPhone);
        $(window).on('hashchange', Methods.appendCelPhone);

    },

    userFormOrder() {
        // Containers
        const $personalDataContainer = $('.profile-detail-form-personal-data');
        const $personalDataName = $('.form-personal-data-name');
        const $personalDataSurname = $('.form-personal-data-surname');
        const $personalDataNickname = $('.form-personal-data-nickname');
        const $personalDataCpf = $('.form-personal-data-cpf');
        const $personalDataBirth = $('.form-personal-data-date-of-birth');
        const $personalDataGender = $('.form-personal-data-gender');

        const $businessDataContainer = $('.profile-detail-form-business-data');
        const $businessDataCorporateName = $('.form-contact-data-corporate-name');
        const $businessDataFancyName = $('.form-contact-data-fancy-name');
        const $businessDataDocument = $('.form-contact-data-business-document');
        const $businessDataStateRegistration = $('.form-contact-data-state-registration');

        const $personalContactContainer = $('.profile-detail-form-contact-data');
        const $personalContactEmail = $('.form-contact-data-email');
        const $personalContactTel = $('.form-contact-data-telephone');
        const $personalContactCel = $('.form-contact-data-cellphone');


        /**
         * Create sub containers
         */
        // Personal Data
        const $personalDataFieldOne = $('<div class="x-form__group">')
            .append($personalDataName)
            .append($personalDataSurname)
            .append($personalDataNickname);
        const $personalDataFieldTwo = $('<div class="x-form__group">')
            .append($personalDataCpf)
            .append($personalDataBirth)
            .append($personalDataGender);

        // Business Data
        const $businessDataFieldOne = $('<div class="x-form__group">')
            .append($businessDataCorporateName)
            .append($businessDataFancyName);
        const $businessDataFieldTwo = $('<div class="x-form__group">')
            .append($businessDataDocument)
            .append($businessDataStateRegistration);

        // Personal Contact
        const $personalContactFieldOne = $('<div class="x-form__group">')
            .append($personalContactEmail)
            .append($personalContactTel)
        const $personalContactFieldTwo = $('<div class="x-form__group">')
            .append($personalContactCel);

        // Create new form
        $personalDataContainer
            .append($personalDataFieldOne)
            .append($personalDataFieldTwo);

        $businessDataContainer
            .append($businessDataFieldOne)
            .append($businessDataFieldTwo);

        $personalContactContainer
            .append($personalContactFieldOne)
            .append($personalContactFieldTwo);
    },

    _getAge(birthDateString) {
        // yyyy/mm/dd        
        var dateParsed = Date.parse(birthDateString);

        var birthDay = new Date(dateParsed);

        var today = new Date();
        var age = today.getFullYear() - birthDay.getFullYear();

        if (today.getMonth() < birthDay.getMonth()) {
            age--;
        }

        if (today.getMonth() == birthDay.getMonth() && today.getDate() < birthDay.getDate()) {
            age--;
        }

        return age
    },

    validaMaiorDeIdade() {
        var events = ['blur'];
        var nasc = document.querySelector('#birthDate');
        events.map(event => {
            if (nasc) {
                nasc.addEventListener(event, function() {
                    var inputAniversario = document.getElementById("birthDate").value.split('/').reverse().join('/');
                    var age = Methods._getAge(inputAniversario);

                    if (isNaN(age)) {
                        alert('Data inválida, Por Favor insirir uma data válida no formato dd/mm/aaaa');
                        window.hasValidAge = false;
                        return false
                    }

                    if (age < 18) {
                        console.log('lt');
                        window.hasValidAge = false;
                        alert('Cadastro permitido apenas para maiores de 18 anos');
                    } else {
                        console.log('gt');
                        window.hasValidAge = true;
                    }
                })
            }
        })
    },

    addressFormOrder() {
        const $addressFormContainer = $('.address-form');

        const $addressOne = $('.address-form-addressee').eq(0);
        const $addressTwo = $('.address-form-addressee').eq(1);
        const $addressType = $('.address-form-address-type');
        const $addressCep = $('.address-form-cep');

        const $addressStreetName = $('.address-form-street-name');
        const $addressNumber = $('.address-form-number');
        const $addressComplement = $('.address-form-complement');

        const $addressReference = $('.address-form-reference');
        const $addressNeighborhood = $('.address-form-neighborhood');
        const $addressState = $('.address-form-state');
        const $addressCity = $('.address-form-city');

        // Build new form
        const $fieldOne = $('<div class="x-form__group">')
            .append($addressOne.addClass('is--first'))
            .append($addressTwo.addClass('is--second'))
            .append($addressType)
            .append($addressCep);

        const $fieldTwo = $('<div class="x-form__group">')
            .append($addressStreetName)
            .append($addressNumber)
            .append($addressComplement);

        const $fieldThree = $('<div class="x-form__group">')
            .append($addressReference)
            .append($addressNeighborhood)
            .append($addressState)
            .append($addressCity);

        $addressFormContainer
            .empty()
            .append($fieldOne)
            .append($fieldTwo)
            .append($fieldThree);
    },

    appendCelPhone() {
        $.get('/no-cache/profileSystem/getProfile').then(data => {
            $.get(`/api/dataentities/CL/search/?_fields=celPhone&_where=email=${ data.Email }`).then(data => {
                $('.vtex-profile-form__field-wrapper.vtex-profile-form__homePhone').after(`
                    <div class='vtex-profile-form__field-wrapper vtex-profile-form__celPhone pb7'>
                        <label class="vtex-input w-100">
                            <span class="vtex-input__label db mb3 w-100 f6 ">Celular</span>
                            <div class="flex vtex-input-prefix__group relative">
                                <input value="${ (data[0]).celPhone || '' }" class="x-celphone js--celphone w-100 ma0 border-box bw1 br2 b--solid outline-0 near-black b--light-gray hover-b--silver bg-white f6 pv3 ph5" maxlength="30" name="celPhone" placeholder="Opcional" type="tel" />
                            </div>
                        </label>
                    </div>
                `);

                if (!$('.js--append-celphone').length) {
                    $('.vtex-profile-form__profile-summary').append(`
                        <div class='js--append-celphone flex-ns flex-wrap'>
                            <div class='mb8 w-50-ns'>
                                <div>
                                    <label class="db c-on-base mb3">Celular</label>
                                    <div class="light c-on-disabled">${ (data[0]).celPhone || '' }</div>
                                </div>
                            </div>
                        </div>
                    `)
                };
            });
            setTimeout(() => {
                if ($('.js--celphone').val() == "") {
                    $('.js--celphone').mask('(99) 99999-9999');
                };
            }, 1000);
        });
    },

    validateUserCelPhone() {
        var storeName = jsnomeLoja;
        $(document).on('blur', '.js--celphone', (ev) => {
            $.ajax({
                type: 'GET',
                url: '/no-cache/profileSystem/getProfile',
                success: function(data) {
                    if (data.IsUserDefined != false) {
                        var clientEmail = data.Email;
                        var jsonUser = {
                            "email": `${clientEmail}`,
                            "celPhone": $('.js--celphone').val(),
                        };

                        $.ajax({
                            headers: Methods._headers(),
                            data: JSON.stringify(jsonUser),
                            type: 'PATCH',
                            dataType: 'json',
                            url: '/api/dataentities/CL/documents',

                            success: function(data) {
                                console.log('Success documents: ', data);
                                console.log('jsonUser: ', jsonUser);
                            },
                            error: function(data) {
                                console.log(data);
                            }
                        });
                    } else {
                        console.log('Não logado: ', data);
                    };
                },
                error: function(data) {
                    console.log(data);
                    console.log("ocorreu um erro.");
                }
            });
        });
    },

    _headers() {
        var headers = {
            "Accept": "application/vnd.vtex.ds.v10+json",
            "Content-Type": "application/json"
        };

        return headers;
    },
};

export default {
    init: Methods.init,
};

function numericKeyboard() {
        
    $('input[name="document"]').prop('type', 'tel');
    $('input[name="celPhone"]').prop('type', 'tel');
    $('input[name="homePhone"]').prop('type', 'tel');
    $('input[name="birthDate"]').prop('type', 'tel');

};

document.addEventListener('DOMContentLoaded', numericKeyboard());
setInterval(function() { numericKeyboard(); }, 1000);