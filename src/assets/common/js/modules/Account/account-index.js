
import AccountMain from './_account__main.js';
import AccountMenu from './_account__menu.js';
import AccountFormOrder from './_account__form-order.js';
import AccountWishlistActions from './_account__wishlist-actions.js';

const init = () => {
    AccountMain.init();
    AccountMenu.init();
    AccountFormOrder.init();
    AccountWishlistActions.init();
};

export default {
    init: init,
};
