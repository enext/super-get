import AppSlider from './_app-slider';

const init = () => {
    AppSlider.init();
};

export default {
    init: init,
};
