const Methods = {
    init() {
        Methods.slider();
    },

    slider() {
        $('.x-flexslick__slider, #x-flexslick2__theTarget1').slick({
            dots:true,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 767,
                    adaptiveHeight: false
                }
            ]
        });
    },
};

export default {
    init: Methods.init,
};
