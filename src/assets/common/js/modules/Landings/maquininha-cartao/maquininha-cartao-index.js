import MaquininhaCartao from './_maquininha-cartao-main';

const init = () => {
    MaquininhaCartao.init();
};

export default {
    init: init,
};