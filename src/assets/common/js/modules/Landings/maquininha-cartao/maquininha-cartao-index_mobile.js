import MaquininhaCartao from './_maquininha-cartao-main_mobile';

const init = () => {
    MaquininhaCartao.init();
};

export default {
    init: init,
};