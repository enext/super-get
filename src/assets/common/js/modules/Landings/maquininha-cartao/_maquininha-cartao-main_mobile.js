const Methods = {
    init() {
      Methods.doShelfText();
      Methods.setScroll();
    },
    doShelfText() {
        const textList = $('.x-comparative__text ul li'),
              advantagesList = $('.x-comparative__ideal ul'),
              svgsList = $('.x-comparative__svgs ul'),
              svgs = $('.x-shelf-machine__product-topics'),
              advantages = $('.x-shelf-machine__product-advantages'),
              text = $('.x-shelf-machine__product-text');
    
          advantages.each(function (index) {
          advantages.eq(index).append(advantagesList.eq(index));
          text.eq(index).html(textList.eq(index).text());
          svgs.eq(index).html(svgsList.eq(index));
        });
    },
    setScroll() {
        $('a.button').click((ev) => {
          ev.preventDefault();
            let id = $('a.button').attr('href');
            let targetOffset = $(id).offset().top;
            $('html, body').animate({scrollTop: targetOffset}, 560);
        });
    },
};
 
export default {
  init: Methods.init,
};
  