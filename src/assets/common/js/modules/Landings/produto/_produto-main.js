const Methods = {
  init() {
    Methods.shelfBuyItem();
    Methods.selectShelfChip();
    Methods.doLogin();
  },
  doLogin() {
    VTEX.vtexHelpers.checkLogin().then(function (user) {
      if(user.IsUserDefined) {
        $(".x-login__options .js--login").parent().addClass("is--logged")
      }
    }),
    $(".x-login__options .js--login").on('click', (ev) => {
      ev.preventDefault();
      const $this = $(ev.currentTarget);
      const $parent = $this.parent();

      if ($parent.hasClass('is--logged')) {
        window.location.href = '/account';

        return false;
      }

      VTEX.closeMenus();
      VTEX.body.addClass('has--loader');
      VTEX.vtexHelpers.checkLogin().fail((user) => {
        if (!user.IsUserDefined) {
          VTEX.vtexHelpers.openPopupLogin(true);
          return false;
        }
      });
    });
  },
  shelfBuyItem() {
    $('body').on('click', '.x-product__buy-button > a', function (ev) {
      ev.preventDefault();
      //    console.log('clicou');
      var _element = $(this);
      var _sku = _element.attr('data-sku');
      var _parents = _element.parent().prev('.x-product__chips');
      var _chips = _element.parents('.x-product').find('.x-product__chips li.is--active');
      var _attachment = _element.parents('.x-product').find('.x-product__chips li.is--active button').attr('data-chip');
      Methods._chipsItem(_parents, _element, _chips, _sku, _attachment);
    });
  },
  _chipsItem(_parents, _element, _chips, _sku, _attachment) {
    if (_chips.length) {
      _element.removeClass('is--disabled');
      _parents.removeClass('is--error');
      Methods._addProductToCart(_sku, _attachment);
    } else {
      _element.addClass('is--disabled');
      _parents.addClass('is--error');
    }
  },
  selectShelfChip() {
    $('.x-product__chips li').click(function () {
      if (!$(this).hasClass('is--active')) {
        $('.x-product__chips li').removeClass('is--active');
        $(this).addClass('is--active');
        $(this).parents('.x-product').find('.x-product__chips').removeClass('is--error');
        $(this).parents('.x-product').find('.x-product__buy-button a').removeClass('is--disabled');
      }
    });
  },

  canSendProduct(_orderForm, _product) {

    const productQtd = _orderForm.items.reduce(($qtd, $item) => {

      if ($item.id == _product.id) {
        return $qtd + $item.quantity;
      }

      return $qtd;

    }, 0);

    if (productQtd >= 3) {
      return false;
    }

    return true;
  },

  sendAttachmentInProduct(orderForm, _product, content, attachmentName) {

    $(document).on("productAddedToCart", function () {
      window.location.href = '/checkout';
    });


    if (!Methods.canSendProduct(orderForm, _product)) {
      alert(`Você só pode ter no máximo 3 itens deste produto no carrinho.\nPara comprar mais de 3 unidades do mesmo equipamento ligue para 4002-4000.`);
    }
    else if (orderForm.items.length !== 0) {

      const productsEquals = [];
      orderForm.items.forEach(($item, $index) => {

        //Id do produto 4 que não tem bandeira
        if ($item.id == 4 && _product.id == 4) {

          productsEquals.push({
            product: $item,
            index: $index
          });

        }
        else if ($item.id === _product.id && $item.attachments[0].content["Chip"] == content["Chip"]) {

          productsEquals.push({
            product: $item,
            index: $index
          });
        }

      });


      // Acrescentar mais um produto que já está no carrinho
      if (productsEquals.length > 0 && productsEquals[0]) {
        const updateItem = {
          index: productsEquals[0].index,
          quantity: productsEquals[0].product.quantity + 1
        };
        vtexjs.checkout.updateItems([updateItem], null, false);
        $(document).trigger("productAddedToCart");
      }
      else {

        vtexjs.checkout.addToCart([_product]).done(orderForm => {

          const newItemPosition = orderForm.items.reduce(($newItemPosition, $item, $index) => {
            try {
              return $item.id == _product.id && $item.attachments.length === 0
                && $item.id !== 4
                ? $index
                : $newItemPosition;
            } catch (error) {
              return $newItemPosition;
            }

          }
            , false);

          if (newItemPosition) {
            vtexjs.checkout.addItemAttachment(newItemPosition, attachmentName, content, null, false);
            $(document).trigger("productAddedToCart");
          }

        });
      }
    }
    else {

      vtexjs.checkout.addToCart([_product]).done(function (orderForm) {
        vtexjs.checkout.addItemAttachment(0, attachmentName, content, null, false);
        $(document).trigger("productAddedToCart");
      });
    }
  },
  _addProductToCart(_sku, _attachment) {
    var _product = {
      id: _sku,
      quantity: 1,
      seller: 1
    };
    var attachmentName = 'Chip';
    var content = { Chip: _attachment };
    vtexjs.checkout.getOrderForm().done(function (orderForm) {
      Methods.sendAttachmentInProduct(orderForm, _product, content, attachmentName);
    });
  },
};

export default {
  init: Methods.init,
};
