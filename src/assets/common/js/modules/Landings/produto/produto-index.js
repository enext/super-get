import BancoOriginal from './_produto-main';

const init = () => {
    BancoOriginal.init();
};

export default {
    init: init,
};