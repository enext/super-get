import BancoOriginal from './_produto-main_mobile';

const init = () => {
    BancoOriginal.init();
};

export default {
    init: init,
};