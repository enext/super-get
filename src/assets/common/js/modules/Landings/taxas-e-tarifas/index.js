export default {
    init() {
        function calculateTotalMoney(value, overtimeNumber, mdr, fee) {
            // {[Valor bruto*(1-MDR)]/N}*{(100%*N)-[[(1+N)*N]/2]*TS}
            // ((C11*(1-E11))/G11)*((100%*G11)-(((1+G11)*G11)/2)*H11))
            if (fee == 0) {
                var totalMoney = value * (1 - mdr);
                totalMoney = totalMoney.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
                return totalMoney;
            }

            if (overtimeNumber <= 3)
                overtimeNumber = 2;
            else if (overtimeNumber <= 6)
                overtimeNumber = 4;
            else
                overtimeNumber = 7;
            var totalMoney = ((value * (1 - mdr)) / overtimeNumber) * ((1 * overtimeNumber) - (((1 + overtimeNumber) * overtimeNumber) / 2) * fee);
            totalMoney = totalMoney.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
            return totalMoney;
        }

        function getOvertime() {
            var overtime = $("#simulation-select-payment").children("option:selected").val();
            if (overtime == "debit" || overtime == "credit" || overtime == "overtimeEmitter")
                return 1;
            else
                return overtime;
        }

        function getPlan() {
            var plan = $("#simulation-select-plan").children("option:selected").val();
            return plan;
        }

        function getValueMoney() {
            var value = $("#simulation-text-value").val();
            if (value != "") {
                value = value.replace(".", "");
                value = value.replace(",", ".");
                value = value.replace("R$", "");
                return value;
            }
            return 0;
        }

        function changeTotalMoney(plan) {
            var overtime = getOvertime();
            var value = getValueMoney();
            if (plan == "2d") {
                var mdr = [0.02, 0.02, 0.02, 0.0559];
                fee = [0, 0, 0, 0.0299];
            }
            else if (plan == "30d") {
                var mdr = [0.02, 0.0309, 0.0309, 0.0379];
                var fee = [0, 0, 0, 0.0299];
            }
            else {
                var mdr = [0.0195, 0.0309, 0.0309, 0.0640];
                var fee = [0, 0, 0, 0];
            }
            $(".simulation-table-debit .simulation-table-totalMoney").html(calculateTotalMoney(value, overtime, mdr[0], fee[0]));
            $(".simulation-table-credit .simulation-table-totalMoney").html(calculateTotalMoney(value, overtime, mdr[1], fee[1]));
            $(".simulation-table-overTimeEmitter .simulation-table-totalMoney").html(calculateTotalMoney(value, overtime, mdr[2], fee[2]));
            $(".simulation-table-overTimeShopkeeper .simulation-table-totalMoney").html(calculateTotalMoney(value, overtime, mdr[3], fee[3]));
        }

        function changeTotalFee(plan) {
            var totalFee = ["2,00%", "2,00%", "2,00%"];
            var overtime = getOvertime();

            if (plan == "2d") {
                totalFee[0] = "2,00%";
                totalFee[1] = "2,00%";
                totalFee[2] = "2,00%";

                if (overtime <= 3) {
                    totalFee.push("9,82%");
                }
                else if (overtime <= 6)
                    totalFee.push("12,65%");
                else
                    totalFee.push("16,88%");
            }
            else if (plan == "30d") {
                totalFee[0] = "2,00%";
                totalFee[1] = "3,09%";
                totalFee[2] = "3,09%";

                if (overtime <= 3)
                    totalFee.push("8,11%");
                else if (overtime <= 6)
                    totalFee.push("10,98%");
                else
                    totalFee.push("15,30%");
            }
            else {
                // case 12t
                totalFee[0] = "1,95%";
                totalFee[1] = "3,09%";
                totalFee[2] = "3,09%";
                totalFee.push("6,40%");
            }
            $(".simulation-table-debit .simulation-table-totalFee").html(totalFee[0]);
            $(".simulation-table-credit .simulation-table-totalFee").html(totalFee[1]);
            $(".simulation-table-overTimeEmitter .simulation-table-totalFee").html(totalFee[2]);
            $(".simulation-table-overTimeShopkeeper .simulation-table-totalFee").html(totalFee[3]);
        }
        function changeMdr(plan) {
            if (plan == "2d") {
                var mdr = ["2,00%", "2,00%", "2,00%", "5,59%"];
            }
            else if (plan == "30d") {
                var mdr = ["2,00%", "3,09%", "3,09%", "3,79%"];
            }
            else {
                var mdr = ["2,00%", "3,09%", "3,09%", "6,40%"];
            }
            $(".simulation-table-debit .simulation-table-mdr").html(mdr[0]);
            $(".simulation-table-credit .simulation-table-mdr").html(mdr[1]);
            $(".simulation-table-overTimeEmitter .simulation-table-mdr").html(mdr[2]);
            $(".simulation-table-overTimeShopkeeper .simulation-table-mdr").html(mdr[3]);
        }
        function changeFee(plan) {
            if (plan == "2d") {
                var fee = ["-", "-", "-", "2,99%"];
            }
            else if (plan == "30d") {
                var fee = ["-", "-", "-", "2,99%"];
            }
            else {
                var fee = ["-", "-", "-", "-"];
            }
            $(".simulation-table-debit .simulation-table-serviceFee").html(fee[0]);
            $(".simulation-table-credit .simulation-table-serviceFee").html(fee[1]);
            $(".simulation-table-overTimeEmitter .simulation-table-serviceFee").html(fee[2]);
            $(".simulation-table-overTimeShopkeeper .simulation-table-serviceFee").html(fee[3]);
        }

        $(function () {
            $('#simulation-text-value').maskMoney({ allowZero: true });
            $("#simulation-select-plan").change(function () {
                var plan = getPlan();
                changeMdr(plan);
                changeFee(plan);
                changeTotalFee(plan);
                changeTotalMoney(plan);
            });
            $("#simulation-select-payment").change(function () {
                var plan = getPlan();
                changeTotalFee(plan);
                changeTotalMoney(plan);
            });
            $("#simulation-text-value").keyup(function () {
                changeTotalMoney(getPlan());
            });
        });
    }
}