import slider from './slider';

const init = () => {
    slider.init();
};

export default {
    init: init,
};
