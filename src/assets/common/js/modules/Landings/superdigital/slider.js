const Methods = {
    init() {
        Methods.slider();
    },

    slider() {
        slick_slider();
        $(window).resize(slick_slider);

        function slick_slider() {
            var wrapper = $(".x-slider-content");
            if ($(".slick-initialized").length) {
                wrapper.slick('unslick');
            }
            wrapper.slick({
                dots: true,
                dotsClass: 'x-dots-container',
                speed: 600,
                infinite: false,
                slidesToShow: 1,
                arrows: false,
                centerMode: true,
                variableWidth: true,
                mobileFirst: true,
                responsive: [{
                    breakpoint: 1000,
                    settings: "unslick"
                }]
            });
        }
    },
};

export default {
    init: Methods.init,
};




