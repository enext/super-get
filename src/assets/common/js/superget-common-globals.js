// Rivets.js
import './utils/rivets-configure';
import './utils/rivets-formatters';
import './utils/rivets-binders';

import Globals from './modules/Globals/globals-index.js';

document.addEventListener('DOMContentLoaded', Globals.init);

document.addEventListener("DOMContentLoaded", function(event) {
    var myLazyLoad = new LazyLoad();
    myLazyLoad.update();
});