export default class VanillaAccordion {
    constructor(data) {
        this.setDefaultData();
        this.setCustomData(data);

        [].map.call(document.querySelectorAll(this.elements), (el, i) => {
            this.els.push({
                wrapper: el,
                trigger: el.querySelector(this.trigger),
                content: el.querySelector(this.content)
            });

            this.els[i].content.style.height = 0;
            this.els[i].content.style.overflow = "hidden";

            if (this.noAria) {
                return false;
            }

            this.els[i].trigger.setAttribute(
                "aria-controls",
                "accordion-content-" + i
            );
            this._setAria(this.els[i], "collapsed");
        });

        this.attachEvents();

        if (this.openFirst) {
            this._setAria(this.els[0], "expanded");
            this.els[0].content.style.height = "auto";
            this.open(this.els[0]);
        }
    }

    transitionendEventName() {
        const el = document.createElement("div");
        const transitions = {
            transition: "transitionend",
            OTransition: "otransitionend",
            MozTransition: "transitionend",
            WebkitTransition: "webkitTransitionEnd"
        };

        for (let key in transitions) {
            if (
                transitions.hasOwnProperty(key) &&
                el.style[key] !== undefined
            ) {
                return transitions[key];
            }
        }
    }

    expand(el) {
        const resetHeight = ev => {
            if (ev.target !== el.content) {
                return false;
            }

            el.content.removeEventListener(this.transitionendEvent, bindEvent);

            if (!el.isOpen) {
                return false;
            }

            requestAnimationFrame(() => {
                el.content.style.transition = "0";
                el.content.style.height = "auto";

                requestAnimationFrame(() => {
                    el.content.style.transition = null;
                    el.content.style.height = null;

                    this.fire("elementOpened", el);
                });
            });
        };

        const bindEvent = resetHeight.bind(this);
        el.content.addEventListener(this.transitionendEvent, bindEvent);

        el.isOpen = true;
        el.content.style.height = el.content.scrollHeight + "px";
        this._setAria(el, "expanded");
    }

    collapse(el) {
        const endTransition = ev => {
            if (ev.target !== el.content) {
                return false;
            }

            el.content.removeEventListener(this.transitionendEvent, bindEvent);

            if (el.isOpen) {
                return false;
            }

            this.fire("elementClosed", el);
        };

        const bindEvent = endTransition.bind(this);
        el.content.addEventListener(this.transitionendEvent, bindEvent);

        el.isOpen = false;
        this._setAria(el, "collapsed");

        requestAnimationFrame(() => {
            el.content.style.transition = "0";
            el.content.style.height = el.content.scrollHeight + "px";

            requestAnimationFrame(() => {
                el.content.style.transition = null;
                el.content.style.height = 0;
            });
        });
    }

    _setAria(el, status) {
        if (this.noAria) {
            return false;
        }

        el.trigger.setAttribute(
            "aria-expanded",
            status === "expanded" ? true : false
        );
        el.content.setAttribute(
            "aria-hidden",
            status === "expanded" ? false : true
        );
    }

    open(el) {
        el.selected = true;
        this.fire("elementSelected", el);
        this.expand(el);
        el.wrapper.classList.add(this.selectedClass);
    }

    close(el) {
        el.selected = false;
        this.fire("elementUnselected", el);
        this.collapse(el);
        el.wrapper.classList.remove(this.selectedClass);
    }

    toggle(el) {
        if (el.selected) {
            this.close(el);
            return false;
        }

        this.open(el);

        if (this.oneAtATime) {
            this.els
                .filter(e => e !== el && e.selected)
                .forEach(e => this.close(e));
        }
    }

    attachEvents() {
        this.els.forEach((el, i) => {
            el.trigger.addEventListener("click", this.toggle.bind(this, el));
        });
    }

    setCustomData(data) {
        const keys = Object.keys(data);

        for (let i = 0, len = keys.length; i < len; i++) {
            this[keys[i]] = data[keys[i]];
        }
    }

    fire(eventName, el) {
        const callbacks = this.events[eventName];

        for (let i = 0, len = callbacks.length; i < len; i++) {
            callbacks[i](el);
        }
    }

    on(eventName, cb) {
        if (!this.events[eventName]) {
            return false;
        }

        this.events[eventName].push(cb);
    }

    setDefaultData() {
        this.els = [];
        this.events = {
            elementSelected: [],
            elementOpened: [],
            elementUnselected: [],
            elementClosed: []
        };
        this.transitionendEvent = this.transitionendEventName();
        this.noAria = true;
        this.oneAtATime = true;
        this.openFirst = false;
        this.selectedClass = "is--selected";
        this.trigger = ".accordion__trigger";
        this.content = ".accordion__content";
    }
}
